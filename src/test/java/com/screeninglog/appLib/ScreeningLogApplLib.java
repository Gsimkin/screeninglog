package com.screeninglog.appLib;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.io.File;
import java.util.Calendar;
import java.util.List;

import com.screeninglog.objLib.ScreeningLogObjLib;
import com.screeninglog.util.Constants;
import com.screeninglog.util.PageUtil;

import org.apache.http.util.Asserts;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import org.openqa.selenium.JavascriptExecutor;
import java.util.List;
import org.openqa.selenium.WebElement;

import javax.management.relation.Role;




public class ScreeningLogApplLib extends ScreeningLogObjLib{

    SoftAssert softAssert;
    WebDriver driver;

    public ScreeningLogApplLib(WebDriver driver, SoftAssert softAssert) {
        PageFactory.initElements(driver, this);
        this.softAssert = softAssert;
        this.driver=driver;
    }


    /***************************************
     * Set login Email Address
     **************************************/
    public void setEmailaddress(String email) {
        if (Account_User_Name_Input_Field.isDisplayed() & Account_User_Name_Input_Field.isEnabled())
        	Account_User_Name_Input_Field.sendKeys(email);

    }

    /**************************************
     * Set Login Password
     ************************************************/

    public void setPassword(String pass) {
        if (Password_Input_Field.isDisplayed() & Password_Input_Field.isEnabled())
            Password_Input_Field.sendKeys(pass);
    }

    /**********************************
     * Sign in button
     *****************************************************/
    public void Next_Button() {
        if (Account_User_Name_Next_Button.isDisplayed() & Account_User_Name_Next_Button.isEnabled())
        	Account_User_Name_Next_Button.click();
    }
    
   
    /****************************************************
     * Email address required validation
     ********************************/
    public void EmailRequired() {
        this.setPassword(Constants.Password);
        this.Next_Button();
        softAssert.assertTrue(Email_RequiredField_Validation_Sign.isDisplayed(), "Email is mandatory field");
    }

    /***********************************
     * Password required field validation
     *****************************************************/
    public void PasswordRequired() {
        this.setEmailaddress(Constants.Email);
        this.Next_Button();

        softAssert.assertTrue(Password_RequiredField_Validation_Sign.isDisplayed(), "Password is mandatory field");

    }

    /********************************
     * both field required validation
     ************************************************************/
    public void mandatoryfields() {
        this.Next_Button();
        softAssert.assertTrue(Email_RequiredField_Validation_Sign.isDisplayed() & Password_RequiredField_Validation_Sign.isDisplayed(),
                "Email and Password is mandatory fields");
    }

    /*****************************************************
     * Required field validation
     ******************************************/
    public void required_FieldValidation() {
        mandatoryfields();
        EmailRequired();
        Password_Input_Field.clear();
        PasswordRequired();
        Email_Input_Field.clear();
    }

    /********************************************
     * Login failed with mismatch data
     **********************************************/
    public void failedlogin(String sEmail, String sPassword) {

        this.setEmailaddress(sEmail);
        this.setPassword(sPassword);
        this.Next_Button();
        softAssert.assertTrue(Validation_Message_Text.isDisplayed(), "incorrect failed login message" + Validation_Message_Text.getText());
        Email_Input_Field.clear();
        Password_Input_Field.clear();
    }

    /*******************************************
     * to do login into system admin user
     **************************************************************/
    public void loginToScreeningLogAdminUser(String sEmail, String sPassword) {
        this.setEmailaddress(sEmail);
        

        if (Account_User_Name_Next_Button.isDisplayed()) {
        	Account_User_Name_Next_Button.click();
        }
        this.Next_Button();
        softAssert.assertTrue(1==1, "Login was not successful."); 
        
        if (Azure_User_Name_Input_Field.isDisplayed() & Azure_User_Name_Input_Field.isEnabled())
        	Azure_User_Name_Input_Field.clear();
        Azure_User_Name_Input_Field.sendKeys("SVC-QA-ADMIN-AUTOMATION@imax.com");
        Azure_Password_Input_Field.sendKeys("ivq6ipcum$33miuu");
        Azure_Signin_button.click();        
        PageUtil.waitForPageAnimation();
        Account_Acceptance_Yes_Button.click(); 
        
        

    }
    
    /*******************************************
     * to do login into system basic user
     **************************************************************/
    public void loginToScreeningLogBasicUser(String sEmail, String sPassword) {
        this.setEmailaddress(sEmail);
        

        if (Account_User_Name_Next_Button.isDisplayed()) {
        	Account_User_Name_Next_Button.click();
        }
        this.Next_Button();
        softAssert.assertTrue(1==1, "Login was not successful.");
        
        if (Azure_User_Name_Input_Field.isDisplayed() & Azure_User_Name_Input_Field.isEnabled())
        	Azure_User_Name_Input_Field.clear();
        Azure_User_Name_Input_Field.sendKeys("SVC-QA-AUTOMATION@imax.com");
        Azure_Password_Input_Field.sendKeys("!t3fzaxtabast#83");
        Azure_Signin_button.click();        
        PageUtil.waitForPageAnimation();
        Account_Acceptance_Yes_Button.click(); 
        
        

    }

    
    
    /*****************************************************
     * Login and Navigation Screening log
     **********************************************/
    public void Link_Element_Presence_Go_To() {
        if (GoTomenu_Text.isDisplayed() & GoTomenu_Text.isEnabled())
        	GoTomenu_Text.click();
        if (CreateReportmenu_Text.isDisplayed() & CreateReportmenu_Text.isEnabled())
        	CreateReportmenu_Text.click();
        if (Quick_Report_Window.getText().contains("Quick Report"))
        	Terminate_Window_X.click();
        if (GoTomenu_Text.isDisplayed() & GoTomenu_Text.isEnabled())
        	GoTomenu_Text.click();
        if (EventHistorymenu_Text.isDisplayed() & EventHistorymenu_Text.isEnabled())
        	EventHistorymenu_Text.click();
        if (EventHistory_Window.getText().contains("Events History"))
        	Event_History_Close_X.click();
        
        if (GoTomenu_Text.isDisplayed() & GoTomenu_Text.isEnabled())
        	GoTomenu_Text.click();
        if (AlarmHistorymenu_Text.isDisplayed() & AlarmHistorymenu_Text.isEnabled())
        	AlarmHistorymenu_Text.click();        
        if (AlarmHistorymenu_Window.getText().contains("Alarm History"))
        	Alarm_History_Close_X.click();
        
        if (GoTomenu_Text.isDisplayed() & GoTomenu_Text.isEnabled())
        	GoTomenu_Text.click();
        if (SensorHistorymenu_Text.isDisplayed() & SensorHistorymenu_Text.isEnabled())
        	SensorHistorymenu_Text.click();        
        if (SensorHistorymenu_Window.getText().contains("Sensor History"))
        	Sensor_History_Close_X.click();
        
        if (GoTomenu_Text.isDisplayed() & GoTomenu_Text.isEnabled())
        	GoTomenu_Text.click();
        if (WindRosemenu_Text.isDisplayed() & WindRosemenu_Text.isEnabled())
        	WindRosemenu_Text.click();        
        if (WindRose_Window.getText().contains("Wind Rose"))
        	WindRose_Close_X.click();
        
        if (GoTomenu_Text.isDisplayed() & GoTomenu_Text.isEnabled())
        	GoTomenu_Text.click();
        if (ERGBookmenu_Text.isDisplayed() & ERGBookmenu_Text.isEnabled())
        	ERGBookmenu_Text.click();        
        
     
    }
    
    /*****************************************************
     * Admin Tools availability/Navigation Functionality/Admin Menu
     **********************************************/
    public void Admin_Tools_Availability() {  
    	
    	         
        if (User_Profile_Link.getText().contains("A2sUa1Xb4qUbdt7GqlbeWynTYYEwxHr6CK9pwUhL2qU") & Top_Nav_Admin_Link.isEnabled())
        	Top_Nav_Admin_Link.click();     	
        	Assert.assertTrue(driver.getCurrentUrl().contains("#/admin"));
        	Top_Nav_Audit_Link.click();
        	Assert.assertTrue(driver.getCurrentUrl().contains("#/audit"));
			Top_Nav_Logs_Link.click();
			Assert.assertTrue(driver.getCurrentUrl().contains("view"));
			Top_Nav_Title_Management_Link.click();
			Assert.assertTrue(driver.getCurrentUrl().contains("#/title"));
			Top_Nav_Dropdown_Values_Link.click();
			Assert.assertTrue(driver.getCurrentUrl().contains("#/field"));
        	Top_Nav_Logout_Link.click();
   
                
    
    }

    public WebElement getUserExist(String searchKeyword){
        try {
            return driver.findElement(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']"));
        }catch (Exception ex){
            return null;
        }
    }

    public boolean checkUserExists(String searchKeyword){
        try {
            return driver.findElements(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']")).size() > 0;
        }catch (Exception ex){
            return false;
        }
    }
    
    
    
    /*****************************************************
     * User Tools availability/Navigation Functionality/User Menu
     **********************************************/
    public void User_Tools_Availability() {               
    	
        if (User_Profile_Link.getText().contains("wW5efv-nVD3BoM0OwDe89MYQKZKpouvnfDAhBNyw4x0") & User_Profile_Link.isEnabled())
        	User_Profile_Link.click();
        	Assert.assertTrue(driver.getCurrentUrl().contains("#/welcome"));
			Top_Nav_Logs_Link.click();
			Assert.assertTrue(driver.getCurrentUrl().contains("view"));
        	Top_Nav_Logout_Link.click();
        	
   
                
    
    }
    
    
    /*****************************************************
     * User Pagination
     * @throws InterruptedException 
     **********************************************/
    public void Pagination_Validation() throws InterruptedException {               
    	
    	List<WebElement> rightarrow = driver.findElements(By.xpath(".//*[@class='fa fa-angle-right']"));

    	try {
    		System.out.println(rightarrow.size());
        for (int i = 0; i < rightarrow.size(); i++) {

           
            rightarrow = driver.findElements(By.xpath(".//*[@class='fa fa-angle-right']"));

            System.out.println(rightarrow.get(i));

            
            rightarrow.get(i).click();
            //driver.findElement(By.xpath(".//*[@class='fa fa-angle-right']")).click();
            Thread.sleep(3000);

            
            //driver.findElement(By.xpath(".//*[@class='fa fa-angle-right']")).click();
        }
    	}catch (Exception e){
    		System.out.println(e.getMessage());
    	}
        
    
    }

    	
    	
    	
    
    
    
    

    /*****************************************************
     * Add New User/Administration 
     **********************************************/
    public void Add_New_User_Administration() {
        Delete_User_Administration();
        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
        	Administration_Menutext.click(); 
       
        if (Users_Menuitem.isDisplayed() & Users_Menuitem.isEnabled()) {
            Users_Menuitem.click();
            Addbutton_Userslist_Screen.click();
            Firstname_Inputbox.sendKeys("Delete");
            Lastname_Inputbox.sendKeys("me");
            Email_Inputbox.sendKeys("gsim@gmail.com");
            Phone_Inputbox.sendKeys("310-301-0291");
            Password_Inputbox.sendKeys("Password1234");
            ConfirmPassword_Inputbox.sendKeys("Password1234");
            Site_Checkbox_Westlake.click();
            Role_Inputbox.click();
            PageUtil.waitForPageAnimation();
            Role_Value.click();
            Savebutton_addnewuser.click();
            softAssert.assertTrue(getUserExist("gsim@gmail.com").isDisplayed());
//            Edit_User_Administration();
            if (Users_List_Window.getText().contains("Users List"))
                Users_List_Window_X.click();
        }
    }

    /*****************************************************
     * Add New User/Administration Form Validation
     **********************************************/
    public void Validate_Add_New_Administration() {
        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();

        if (Users_Menuitem.isDisplayed() & Users_Menuitem.isEnabled()) {
            Users_Menuitem.click();
            Addbutton_Userslist_Screen.click();
            Savebutton_addnewuser.click();
            softAssert.assertTrue(First_Name_Error.isDisplayed());
            softAssert.assertTrue(Last_Name_Error.isDisplayed());
            softAssert.assertTrue(Email_Error.isDisplayed());
            softAssert.assertTrue(Password_Error.isDisplayed());
            softAssert.assertTrue(Confirm_Password_Error.isDisplayed());
            softAssert.assertTrue(Sites_Error.isDisplayed());
            softAssert.assertTrue(Role_Error.isDisplayed());
            Add_User_Window_X.click();


            Addbutton_Userslist_Screen.click();
            Savebutton_addnewuser.click();
            Password_Inputbox.sendKeys("123");
            ConfirmPassword_Inputbox.sendKeys("123");
            PageUtil.waitForPageAnimation();
            softAssert.assertTrue(Password_Error.isDisplayed(), "Password validation not working");
            Add_User_Window_X.click();

            Addbutton_Userslist_Screen.click();
            Savebutton_addnewuser.click();
            Email_Inputbox.sendKeys("123");
            Password_Inputbox.sendKeys("Password1234");
            PageUtil.waitForPageAnimation();
            softAssert.assertTrue(Email_Error.isDisplayed(), "Email validation not working");
            Add_User_Window_X.click();
        }

        if (Users_List_Window.getText().contains("Users List"))
            Users_List_Window_X.click();
    }
    /*****************************************************
     * Edit User/Administration 
     **********************************************/
    public void Edit_User_Administration() {
        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled()) {
            Administration_Menutext.click();
        }

        if (Users_Menuitem.isDisplayed() & Users_Menuitem.isEnabled()) {
            Users_Menuitem.click();
        }
        Actions act = new Actions(driver);
        act.doubleClick(getUserExist("gsim@gmail.com")).build().perform();
        PageUtil.waitForPageAnimation();
        Firstname_Inputbox.sendKeys("11");
        Savebutton_addnewuser.click();
        softAssert.assertTrue(checkUserExists("Delete11"), "User editing was not successful");
        if (Users_List_Window.getText().contains("Users List"))
            Users_List_Window_X.click();
    }
    
    /*****************************************************
     * Delete User/Administration 
     **********************************************/
    public void Delete_User_Administration() {
        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled()) {
            Administration_Menutext.click();
        }
        if (Users_Menuitem.isDisplayed() & Users_Menuitem.isEnabled()) {
            Users_Menuitem.click();
        }
        WebElement userRow = getUserExist("gsim@gmail.com");
        if(userRow!=null) {
            userRow.click();
            Delete_User_Button.click();
            Yes_Button.click();
            PageUtil.waitForPageAnimation();
            softAssert.assertFalse(checkUserExists("gsim@gmail.com"),
                    "User with email address gsim@gmail.com still exists");
        }
        if (Users_List_Window.getText().contains("Users List"))
        	Users_List_Window_X.click();
    }
    
    /*****************************************************
     * Link availability/Navigation Functionality/Administration Menu
     **********************************************/
    public void Link_Element_Presence_SaferAdmin() {
        if (Safer_Admin_Menutext.isDisplayed() & Safer_Admin_Menutext.isEnabled())
        	Safer_Admin_Menutext.click(); 
        if (Reset_Site_Units_Menutext.isDisplayed() & Reset_Site_Units_Menutext.isEnabled())
        	Reset_Site_Units_Menutext.click();
        if (Safer_Admin_Menutext.isDisplayed() & Safer_Admin_Menutext.isEnabled())
        	Safer_Admin_Menutext.click(); 
        if (My_Profile_Menutext.isDisplayed() & My_Profile_Menutext.isEnabled())
        	My_Profile_Menutext.click();
        if (My_Profile_Window.getText().contains("My Profile"))
        	My_Profile_Window_X.click();
        if (Safer_Admin_Menutext.isDisplayed() & Safer_Admin_Menutext.isEnabled())
        	Safer_Admin_Menutext.click();
        if (Change_Password_Menutext.isDisplayed() & Change_Password_Menutext.isEnabled())
        	Change_Password_Menutext.click();
        if (Change_Password_Window.getText().contains("Change Password"))
        	Change_Password_Window_X.click();
        if (Safer_Admin_Menutext.isDisplayed() & Safer_Admin_Menutext.isEnabled())
        	Safer_Admin_Menutext.click();
        if (Logout_Menutext.isDisplayed() & Logout_Menutext.isEnabled())
        	Logout_Menutext.click();
                
    }
    
    
    /*****************************************************
     * Phone Number Site Change/Administration Menu
     **********************************************/
    public void Phone_Number_Site_Change_SaferAdmin() {
        if (Safer_Admin_Menutext.isDisplayed() & Safer_Admin_Menutext.isEnabled())
        	Safer_Admin_Menutext.click();  
        if (My_Profile_Menutext.isDisplayed() & My_Profile_Menutext.isEnabled())
        	My_Profile_Menutext.click();
        if (My_Profile_Window.getText().contains("My Profile"))
        	My_Profile_Phone_Number_Input.clear();
        	My_Profile_Phone_Number_Input.sendKeys("1800 555 5555");
        	driver.findElements(By.cssSelector("input:not(:checked)[type='checkbox']"));        	
        	My_Profile_Sites_SimiValley.click();
        	Change_Password_Save_Button.click();
        	
        	if (Safer_Admin_Menutext.isDisplayed() & Safer_Admin_Menutext.isEnabled())
            	Safer_Admin_Menutext.click();  
            if (My_Profile_Menutext.isDisplayed() & My_Profile_Menutext.isEnabled())
            	My_Profile_Menutext.click();
            
            My_Profile_Phone_Number_Input.getText().contains("1800 555 5555");
            My_Profile_Sites_SimiValley.isEnabled();
            My_Profile_Window_X.click();
       
                
    }
    
    
    /*****************************************************
     * Change Password
     **********************************************/
    public void Change_Password_SaferAdmin() {
        if (Safer_Admin_Menutext.isDisplayed() & Safer_Admin_Menutext.isEnabled())
        	Safer_Admin_Menutext.click(); 
        
        if (Change_Password_Menutext.isDisplayed() & Change_Password_Menutext.isEnabled())
        	Change_Password_Menutext.click();
        if (Change_Password_Window.getText().contains("Change Password"))
        	Change_Password_Old_Password_Input.sendKeys(ScreeningLogObjLib.password);
        	Change_Password_New_Password_Input.sendKeys("New12345");
        	Change_Password_Confirm_Password_Input.sendKeys("New12345");
        	Change_Password_Save_Button.click();	
            PageUtil.waitForPageAnimation();

        if (Safer_Admin_Menutext.isDisplayed() & Safer_Admin_Menutext.isEnabled())
        	Safer_Admin_Menutext.click();
        if (Logout_Menutext.isDisplayed() & Logout_Menutext.isEnabled())
        	Logout_Menutext.click();
    }
        
        /*****************************************************
         * Set New Password
         **********************************************/
        public void Set_New_Password_SaferAdmin() {
            if (Safer_Admin_Menutext.isDisplayed() & Safer_Admin_Menutext.isEnabled())
            	Safer_Admin_Menutext.click(); 
            
            if (Change_Password_Menutext.isDisplayed() & Change_Password_Menutext.isEnabled())
            	Change_Password_Menutext.click();
            if (Change_Password_Window.getText().contains("Change Password"))
            	Change_Password_Old_Password_Input.sendKeys("New12345");
            	Change_Password_New_Password_Input.sendKeys(ScreeningLogObjLib.password);
            	Change_Password_Confirm_Password_Input.sendKeys(ScreeningLogObjLib.password);
            	Change_Password_Save_Button.click();
                PageUtil.waitForPageAnimation();

            if (Safer_Admin_Menutext.isDisplayed() & Safer_Admin_Menutext.isEnabled())
            	Safer_Admin_Menutext.click();
            if (Logout_Menutext.isDisplayed() & Logout_Menutext.isEnabled())
            	Logout_Menutext.click();
        
        
        
                
    }
    
    
    /*****************************************************
     * Link availability/Navigation Functionality/Map Function Tools
     **********************************************/
    public void Link_Element_Presence_Map_Function_Tools() {
        if (Default_Site_View.isDisplayed() & Default_Site_View.isEnabled())
        	Default_Site_View.click(); 
        if (Map_Items.isDisplayed() & Map_Items.isEnabled())
        	Map_Items.click();
        if (Map_Items_Window.getText().contains("Map Items"))
        	Map_Items_Window_X.click();
        if (Quick_Report.isDisplayed()& Quick_Report.isEnabled())
        	Quick_Report.click();
        	Quick_Report_Window_X.click();
        	
         
    }
    
    
    
    /*****************************************************
     * Add New Organization/Administration 
     **********************************************/
    public void Validate_Add_New_Org_Administration() {
        Delete_Org_Administration();
        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
        	Administration_Menutext.click(); 
       
                
        if (Organization_Menutext.isDisplayed() & Organization_Menutext.isEnabled()) {
        	Organization_Menutext.click();
        	Addbutton_Org_Screen.click();
        	Name_Org_Screen.sendKeys("Org1");
        	Savebutton_addneworg.click();
            softAssert.assertTrue(getOrgExist("Org1").isDisplayed());

            if (Org_List_Window.getText().contains("Organizations List"))
            	Org_List_Window_X.click();
        }
    }
    
    /*****************************************************
     * Edit Organization/Administration 
     **********************************************/
    public void Edit_Org_Administration() {
        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled()) {
            Administration_Menutext.click();
        }

        if (Organization_Menutext.isDisplayed() & Organization_Menutext.isEnabled()) {
        	Organization_Menutext.click();
        }
        Actions act = new Actions(driver);
        act.doubleClick(getOrgExist("Org1")).build().perform();
        PageUtil.waitForPageAnimation();
        Name_Org_Screen.sendKeys("TEST");
        Savebutton_addneworg.click();
        softAssert.assertTrue(checkUserExists("Org1TEST"), "Org editing was not successful");
        if (Org_List_Window.getText().contains("Organizations List"))
        	Org_List_Window_X.click();
    }
    
    /*****************************************************
     * Delete Organization/Administration 
     **********************************************/
    public void Delete_Org_Administration() {
        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled()) {
            Administration_Menutext.click();
        }
        if (Organization_Menutext.isDisplayed() & Organization_Menutext.isEnabled()) {
        	Organization_Menutext.click();
        }
        WebElement userRow = getOrgExist("Org1TEST");
        if(userRow!=null) {
        	Actions action = new Actions(driver);
            action.doubleClick(userRow).perform();
            Delete_Org_Button.click();
            Yes_Button.click();
            PageUtil.waitForPageAnimation();
            softAssert.assertFalse(checkOrgExists("Org1TEST"),
                  "Org with this title Org1 still exists");
        }
        if (Org_List_Window.getText().contains("Organizations List"))
        	Org_List_Window_X.click();
    }
    
    
    public WebElement getOrgExist(String searchKeyword){
        try {
            return driver.findElement(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']"));
        }catch (Exception ex){
            return null;
        }
    }

    public boolean checkOrgExists(String searchKeyword){
        try {
            return driver.findElements(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']")).size() > 0;
        }catch (Exception ex){
            return false;
        }
    }
    
    /*****************************************************
     * Add New DAQ/Administration 
     **********************************************/
    public void Add_New_DAQ_Administration() {
        //Delete_DAQ_Administration();
    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();
        
        Actions act = new Actions(driver);
        
        act.moveToElement(Data_Source_Menutext).build().perform();
        if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
        	act.moveToElement(DAQ_Menutext).build().perform();	
        if (DAQ_Menutext.isDisplayed() & DAQ_Menutext.isEnabled())
        	DAQ_Menutext.click();
         	Addbutton_DAQlist_Screen.click();                        
	        DAQname_Inputbox.sendKeys("DAQ3");
	        DAQ_Site_Value.click();
	        Save_DAQ_Create_Button.click();
            DAQ_List_Window_X.click();
           
            
            if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
                Administration_Menutext.click();
            act.moveToElement(Data_Source_Menutext).build().perform();
            if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
            	act.moveToElement(DAQ_Menutext).build().perform();	
            if (DAQ_Menutext.isDisplayed() & DAQ_Menutext.isEnabled())
            	DAQ_Menutext.click();
            
            
            softAssert.assertTrue(getDAQExist("DAQ3").isDisplayed());
            
            if (DAQ_List_Window.getText().contains("DAQ List"))
            	DAQ_List_Window_X.click();

            }   
        
    
    
    /*****************************************************
     * Add New DAQ/Administration Form Validation
     **********************************************/
    public void Validate_Add_New_DAQ_Administration() {
        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();
        
        Actions act = new Actions(driver);
        
        act.moveToElement(Data_Source_Menutext).build().perform();
        if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
        	act.moveToElement(DAQ_Menutext).build().perform();	
        if (DAQ_Menutext.isDisplayed() & DAQ_Menutext.isEnabled())
        	DAQ_Menutext.click();
               	
        	Addbutton_DAQlist_Screen.click();        	
        	Savebutton_addnewDAQ.click();
            softAssert.assertTrue(Name_Error_DAQ.isDisplayed());
            softAssert.assertTrue(DAQ_Sites_Error.isDisplayed());
            Add_DAQ_List_Window_X.click();
            DAQ_List_Window_X.click();

        }

       
    
    /*****************************************************
     * Edit DAQ/Administration 
     **********************************************/
    public void Edit_DAQ_Administration() {
    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();
        
        Actions act = new Actions(driver);
        
        act.moveToElement(Data_Source_Menutext).build().perform();
        if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
        	act.moveToElement(DAQ_Menutext).build().perform();	
        if (DAQ_Menutext.isDisplayed() & DAQ_Menutext.isEnabled())
        	DAQ_Menutext.click();
        Actions act2 = new Actions(driver);
        act2.doubleClick(getDAQExist("DAQ3")).build().perform();
        PageUtil.waitForPageAnimation();
        DAQname_Inputbox.sendKeys("Test");
        Save_DAQ_Create_Button.click();
        PageUtil.waitForPageAnimation();
        DAQ_List_Window_X.click();
        PageUtil.waitForPageAnimation();
        
        
        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();
        
        Actions act3 = new Actions(driver);
        
        act.moveToElement(Data_Source_Menutext).build().perform();
        if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
        	act3.moveToElement(DAQ_Menutext).build().perform();	
        if (DAQ_Menutext.isDisplayed() & DAQ_Menutext.isEnabled())
        	DAQ_Menutext.click();
        softAssert.assertTrue(checkDAQExists("DAQ3Test"), "DAQ editing was not successful");
        if (DAQ_List_Window.getText().contains("DAQ List"))
        	DAQ_List_Window_X.click();;
    }
    
    /*****************************************************
     * Delete DAQ/Administration 
     **********************************************/
    public void Delete_DAQ_Administration() {
    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();
        
        Actions act = new Actions(driver);
        
        act.moveToElement(Data_Source_Menutext).build().perform();
        if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
        	act.moveToElement(DAQ_Menutext).build().perform();	
        if (DAQ_Menutext.isDisplayed() & DAQ_Menutext.isEnabled())
        	DAQ_Menutext.click();
               
        WebElement DAQRow = getDAQExist("DAQ3Test");
        if(DAQRow!=null) {
            DAQRow.click();
            
            Actions action = new Actions(driver);
            action.doubleClick(DAQRow).perform();
            DAQ_Delete_Button.click();
            DAQ_Yes_Button.click();
           
            PageUtil.waitForPageAnimation();
            softAssert.assertFalse(checkDAQExists("DAQ3Test"),
                    "DAQ with DAQ3Test still exists");
        }
        if (DAQ_List_Window.getText().contains("DAQ List"))
        	DAQ_List_Window_X.click();;
    }
    
    public WebElement getDAQExist(String searchKeyword){
        try {
            return driver.findElement(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']"));
        }catch (Exception ex){
            return null;
        }
    }

    public boolean checkDAQExists(String searchKeyword){
        try {
            return driver.findElements(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']")).size() > 0;
        }catch (Exception ex){
            return false;
        }
    }
    
    /*****************************************************
     * Add New site/Administration 
     **********************************************/
    public void Add_New_Site_Administration() {
        
        
        /*****************************************************
         * Create New Organization Prior Site Creation
         **********************************************/
    	PageUtil.waitForPageAnimation();
        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
        	Administration_Menutext.click(); 
       
                
        if (Organization_Menutext.isDisplayed() & Organization_Menutext.isEnabled()) {
        	Organization_Menutext.click();
        	Addbutton_Org_Screen.click();
        	
        	Name_Org_Screen.sendKeys("Org5");
        	Savebutton_addneworg.click();
            softAssert.assertTrue(getOrgExist("Org5").isDisplayed());
            Add_Site_Button.click();
            Sitename_Inputbox.sendKeys("Site9");
            PageUtil.waitForPageAnimation();
        	Surf_Inputbox.sendKeys("0.33");
        	PageUtil.waitForPageAnimation();
            Savebutton_addnewsite.click();
            PageUtil.waitForPageAnimation();
            OK_Site_Created_Button.click();
            PageUtil.waitForPageAnimation();
            Add_New_User_X.click();
            User_List_for_Org_X.click();
            PageUtil.waitForPageAnimation();

        }
        
       
        PageUtil.waitForPageAnimation();
            if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            	Administration_Menutext.click(); 
           
            if (Sites_Menutext.isDisplayed() & Sites_Menutext.isEnabled()) {
            	Sites_Menutext.click();
            
                

            softAssert.assertTrue(getSiteExist("Site9").isDisplayed());
            
            if (Sites_List_Window.getText().contains("Sites List"))
            	Sites_List_Window_X.click();
            PageUtil.waitForPageAnimation();

            }   
        }
    
    
    /*****************************************************
     * Add New Site/Administration Form Validation
     **********************************************/
    public void Validate_Add_New_Site_Administration() {
    	PageUtil.waitForPageAnimation();
        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();

        if (Sites_Menutext.isDisplayed() & Sites_Menutext.isEnabled()) {
        	Sites_Menutext.click();        	
        	Addbutton_Siteslist_Screen.click();        	
        	Savebutton_addnewsite.click();
            softAssert.assertTrue(Name_Error.isDisplayed());
            softAssert.assertTrue(Surf_Error.isDisplayed());
            Sites_List_Window_X.click();
            PageUtil.waitForPageAnimation();

        }

       
    }
    /*****************************************************
     * Edit Site/Administration 
     **********************************************/
    public void Edit_Site_Administration() {
    	PageUtil.waitForPageToLoad();
        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled()) {
            Administration_Menutext.click();
        }

        if (Sites_Menutext.isDisplayed() & Sites_Menutext.isEnabled()) {
        	Sites_Menutext.click();
        }
        Actions act = new Actions(driver);
        act.doubleClick(getSiteExist("Site9")).build().perform();
        PageUtil.waitForPageToLoad();
        Sitename_Inputbox.sendKeys("Test");
        PageUtil.waitForPageToLoad();
        Savebutton_addnewsite.click();
        PageUtil.waitForPageToLoad();
        OK_Site_Created_Button.click();
        PageUtil.waitForPageToLoad();
        Add_New_User_X.click();
        User_List_for_Org_X.click();
        PageUtil.waitForPageToLoad();
        
        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled()) {
            Administration_Menutext.click();
        }

        if (Sites_Menutext.isDisplayed() & Sites_Menutext.isEnabled()) {
        	Sites_Menutext.click();
        }
        softAssert.assertTrue(checkUserExists("Site9Test"), "Site editing was not successful");
        if (Sites_List_Window.getText().contains("Sites List"))
        	Sites_List_Window_X.click();
        PageUtil.waitForPageToLoad();
    }
    
    /*****************************************************
     * Delete Site/Administration 
     **********************************************/
    public void Delete_Site_Administration() {
    	PageUtil.waitForPageToLoad();
    	
    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled()) {
            Administration_Menutext.click();
        }

        if (Sites_Menutext.isDisplayed() & Sites_Menutext.isEnabled()) {
        	Sites_Menutext.click();
        }
        Actions act = new Actions(driver);
        act.doubleClick(getSiteExist("Site9Test")).build().perform();
        PageUtil.waitForPageToLoad();
        Delete_Site_Button.click();
        Yes_Button.click();
        PageUtil.waitForPageToLoad();

        
    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
        	Administration_Menutext.click(); 
       
                
        if (Organization_Menutext.isDisplayed() & Organization_Menutext.isEnabled()) {
        	Organization_Menutext.click();
        	
        	softAssert.assertTrue(getOrgExist("Org5").isDisplayed());
        	
        	Actions act2 = new Actions(driver);
            act2.doubleClick(getOrgExist("Org5")).build().perform();
            PageUtil.waitForPageToLoad();
            Delete_Org_Button.click();
            Yes_Button.click();
            Terminate_Org_List_Window_X.click();
            PageUtil.waitForPageToLoad();
        	
    	
    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled()) {
            Administration_Menutext.click();
            
                       
        }
        if (Sites_Menutext.isDisplayed() & Sites_Menutext.isEnabled()) {
        	Sites_Menutext.click();
        }
        WebElement userRow = getSiteExist("Site9Test");
        if(userRow!=null) {
            userRow.click();
          
            PageUtil.waitForPageToLoad();
            softAssert.assertFalse(checkSiteExists("Site5Test"),
                    "Site with Site1Test still exists");
        }
        if (Sites_List_Window.getText().contains("Sites List"))
        	Sites_List_Window_X.click();
    }
    }
    
    public WebElement getSiteExist(String searchKeyword){
        try {
            return driver.findElement(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']"));
        }catch (Exception ex){
            return null;
        }
    }

    public boolean checkSiteExists(String searchKeyword){
        try {
            return driver.findElements(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']")).size() > 0;
        }catch (Exception ex){
            return false;
        }
    }
    
    /*****************************************************
     * Add New Sensor/Administration 
     **********************************************/
    public void Add_New_Sensor_Administration() {
        
        	Sensor_Interface_Add_Button.click(); 
        	Type_Inputbox.click();
        	Sensor_Type_Value.click();
        	Sensor_Interface_Name_Field.getText().contains("Modbus");
        	
        	Add_DAQ_Button.click();
        	PageUtil.waitForPageAnimation();
        	Sensor_DAQname_Inputbox.sendKeys("SensorDAQ1");
        	Save_DAQ_Create_Button.click();
	        PageUtil.waitForPageAnimation();
	        Sensor_Interface_Add_Port_Button.click();
	        Port_Type_Inputbox.click();
	        Sensor_Interface_Port_Type_Value.click();
	        Sensor_Interface_Port_IP_Input.sendKeys("10.101.101.1");
	        Sensor_Interface_Port_Save_Button.click();
	        Sensor_Interface_DeviceID_Input_Field.sendKeys("Test1");
	        Sensor_Interface_Encoding_Field.click();
	        Sensor_Interface_Encoding_Value.click();
	        Sensor_Interface_AddressType_Field.click();
	        Sensor_Interface_AddressType_Value.click();
	        Sensor_Interface_NumericFormat_Field.click();
	        Sensor_Interface_NumericFormat_Value.click();
        	
	        Sensor_Interface_Save_Button.click();
	        PageUtil.waitForPageAnimation();
                        
            
            softAssert.assertTrue(getDAQExist("SensorDAQ1").isDisplayed());
            
            if (Sensor_Interfaces_List_Window.getText().contains("SensorDAQ1"))
            	Sensor_Interfaces_List_Window_X.click();

            }   
    
    
    /*****************************************************
     * Add New Sensor/Administration Form Validation
     **********************************************/
    public void Validate_Add_New_Sensor_Administration() {
    	PageUtil.waitForPageAnimation();
    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();
        
        Actions act = new Actions(driver);
        
        act.moveToElement(Data_Source_Menutext).build().perform();
        if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
        	act.moveToElement(DAQ_Menutext).build().perform();	
        if (Sensor_Interfaces_Menutext.isDisplayed() & Sensor_Interfaces_Menutext.isEnabled())
        	Sensor_Interfaces_Menutext.click();
        	Sensor_Interface_Add_Button.click(); 
        	Sensor_Interface_Save_Button.click();
        	softAssert.assertTrue(Sensor_Interface_Type_Field_Error.isDisplayed());
        	softAssert.assertTrue(Sensor_Interface_Name_Field_Error.isDisplayed());
        	//softAssert.assertTrue(Sensor_Interface_DAQ_Field_Error.isDisplayed());
        	Type_Inputbox.click();
        	Sensor_Type_Value.click();
        	Sensor_Interface_Name_Field.getText().contains("Modbus");
        	Sensor_Interface_Save_Button.click();
        	
        	//softAssert.assertTrue(Sensor_Interface_Port_Field_Error.isDisplayed());
        	softAssert.assertTrue(Sensor_Interface_Encoding_Field_Error.isDisplayed());
        	softAssert.assertTrue(Sensor_Interface_AddressType_Field_Error.isDisplayed());
        	softAssert.assertTrue(Sensor_Interface_NumericFormat_Field_Error.isDisplayed());
        	        	
        	Add_Sensor_Window_X.click();
            PageUtil.waitForPageAnimation();

        }

       
    
    /*****************************************************
     * Edit Sensor/Administration 
     **********************************************/
    public void Edit_Sensor_Administration() {
    	PageUtil.waitForPageAnimation();
    	
        Actions act2 = new Actions(driver);
        act2.doubleClick(getSensorExist("SensorDAQ1")).build().perform();
        PageUtil.waitForPageToLoad();
        Sensor_Interface_Name_Input_Field.sendKeys("SensorDAQ1Edit");
        Sensor_Interface_Save_Button.click();
        PageUtil.waitForPageAnimation();
        
        
        softAssert.assertTrue(checkSensorExists("ModbusSensorDAQ1Edit"), "Sensor editing was not successful");
        if (Sensor_Interfaces_List_Window.getText().contains("Sensor Interfaces"))
        	Sensor_Interfaces_List_Window_X.click();
        PageUtil.waitForPageToLoad();
    }
    
    /*****************************************************
     * Delete Sensor/Administration 
     **********************************************/
    public void Delete_Sensor_Administration() {
    	PageUtil.waitForPageAnimation();
    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();
        
        Actions act = new Actions(driver);
        
        act.moveToElement(Data_Source_Menutext).build().perform();
        if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
        	act.moveToElement(DAQ_Menutext).build().perform();	
        if (Sensor_Interfaces_Menutext.isDisplayed() & Sensor_Interfaces_Menutext.isEnabled())
        	Sensor_Interfaces_Menutext.click();

    	
       Actions act2 = new Actions(driver);
       act2.click(getSensorExist("ModbusSensorDAQ1Edit")).build().perform();
        Delete_Sensor_Button.click();
        Yes_Button.click();
        PageUtil.waitForPageToLoad();
        	
    	
        WebElement userRow = getSensorExist("ModbusSensorDAQ1Edit");
        if(userRow!=null) {
            userRow.click();
          
            PageUtil.waitForPageToLoad();
            softAssert.assertFalse(checkSensorExists("ModbusSensorDAQ1Edit"),
                    "Sensor  still exists");
        }
        
        if (Sensor_Interfaces_List_Window.getText().contains("Sensor Interfaces"))
        	Sensor_Interfaces_List_Window_X.click();
        PageUtil.waitForPageToLoad();
        
        /******************************************************************************************
         * Delete DAQ after each Sensor Delete Since new ones are created on every Sensor creation 
         *******************************************************************************************/
        
        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();
        
        
        act.moveToElement(Data_Source_Menutext).build().perform();
        if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
        	act.moveToElement(DAQ_Menutext).build().perform();	
        if (DAQ_Menutext.isDisplayed() & DAQ_Menutext.isEnabled())
        	DAQ_Menutext.click();
        
        WebElement DAQRow = getDAQExist("SensorDAQ1");
        if(DAQRow!=null) {
            DAQRow.click();
            
            Actions action = new Actions(driver);
            action.doubleClick(DAQRow).perform();
            DAQ_Delete_Button.click();
            DAQ_Yes_Button.click();

            PageUtil.waitForPageAnimation();
            softAssert.assertFalse(checkDAQExists("SensorDAQ1"),
                    "DAQ with SensorDAQ1 still exists");
        }
        if (DAQ_List_Window.getText().contains("DAQ List"))
        	DAQ_List_Window_X.click();;
    }
    
    
    public WebElement getSensorExist(String searchKeyword){
        try {
            return driver.findElement(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']"));
        }catch (Exception ex){
            return null;
        }
    }

    public boolean checkSensorExists(String searchKeyword){
        try {
            return driver.findElements(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']")).size() > 0;
        }catch (Exception ex){
            return false;
        }
    }
    
    /*****************************************************
     * Add New Chemical/Administration 
     **********************************************/
    public void Add_New_Chemical_Administration() {
        
    	    	PageUtil.waitForPageAnimation();
    	    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
    	            Administration_Menutext.click();
    	        
    	        Actions act = new Actions(driver);
    	        
    	        act.moveToElement(Current_Site_Menutext).build().perform();
    	        if (Current_Site_Menutext.isDisplayed() & Current_Site_Menutext.isEnabled())
    	        	act.moveToElement(Chemicals_Menutext).build().perform();
    	        if (Chemicals_Menutext.isDisplayed() & Chemicals_Menutext.isEnabled())
    	        	Chemicals_Menutext.click();
    	        	
    	        	        
    	        if (Chemicals_Window.isDisplayed() & Chemicals_Window.isEnabled())
    	        	PageUtil.waitForPageToLoad();
    	        	Chemicals_Add_Button.click();	        
    	        	Chemimcal_Category_Field.click();
    	        	Chemimcal_Category_Field_Value.click();
    	        	Chemical_Name_Input.sendKeys("Test Chemical");
    	        	Chemical_CAS_Input.sendKeys("CAS Test");
    	        	Chemical_UNDOT_Input.sendKeys("UNDOT Test");
    	        	Chemical_Formula_Input.sendKeys("Chemical_Formula Test");
    	        	Chemical_Alternate_Name_Input.sendKeys("Alternative Name Test");
    	        	
    	        	Chemicals_Window_Particulate_Properties_Tab.click();
    	        	
    	        	Chemimcal_Density_Field.sendKeys("22");
    	        	Chemimcal_Form_Field.click();
    	        	Chemimcal_Form_Field_Value.click();
    	        	Chemimcal_Diamter_Field.click();
    	        	Chemimcal_Diamter_Field_Value.click();
    	        	Chemimcal_Mean_Diamter_Field.sendKeys("1");
    	        	
    	        	    	        	
    	        	Save_Chemical_Button.click();
    	        	
    	        	PageUtil.waitForPageAnimation();
                        
            
            softAssert.assertTrue(getChemicalExist("Test Chemical").isDisplayed());
            
            if (Chemicals_Window.getText().contains("Chemicals"))
            	Chemicals_Window_X.click();

            }   
    
    
    /*****************************************************
     * Add New Chemical/Administration Form Validation
     **********************************************/
    public void Validate_Add_New_Chemcial_Administration() {
    	PageUtil.waitForPageAnimation();
    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();
        
        Actions act = new Actions(driver);
        
        act.moveToElement(Current_Site_Menutext).build().perform();
        if (Current_Site_Menutext.isDisplayed() & Current_Site_Menutext.isEnabled())
        	act.moveToElement(Chemicals_Menutext).build().perform();
        if (Chemicals_Menutext.isDisplayed() & Chemicals_Menutext.isEnabled())
        	Chemicals_Menutext.click();
        	
        	        
        if (Chemicals_Window.isDisplayed() & Chemicals_Window.isEnabled())
        	PageUtil.waitForPageToLoad();
        	Chemicals_Add_Button.click();
        	Save_Chemical_Button.click();
           	softAssert.assertTrue(Chemical_Name_Field_Error.isDisplayed());
           	
           	       	
           	Add_Chemicals_Window_X.click();        	
           	Chemicals_Window_X.click();
            PageUtil.waitForPageAnimation();

        }

       
    
    /*****************************************************
     * Validate Chemical/Administration 
     **********************************************/
    public void Validate_Chemical_Values_Administration() {
    	PageUtil.waitForPageAnimation();
    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();
        
        Actions act = new Actions(driver);
        
        act.moveToElement(Current_Site_Menutext).build().perform();
        if (Current_Site_Menutext.isDisplayed() & Current_Site_Menutext.isEnabled())
        	act.moveToElement(Chemicals_Menutext).build().perform();
        if (Chemicals_Menutext.isDisplayed() & Chemicals_Menutext.isEnabled())
        	Chemicals_Menutext.click();
        	
        	        
        if (Chemicals_Window.isDisplayed() & Chemicals_Window.isEnabled())
        	PageUtil.waitForPageToLoad();
        	Actions act2 = new Actions(driver);
        	act2.doubleClick(getChemicalExist("Test Chemical")).build().perform();
        	PageUtil.waitForPageAnimation();
        	String value = Chemical_Name_Input.getAttribute("innerHtml");
        	if (Chemical_Name_Input.getAttribute("value").contains("Test Chemical")) {
        		softAssert.assertTrue(Chemical_Name_Input.isDisplayed(), "Test Chemical");
        	if (Chemical_CAS_Input.getAttribute("value").contains("CAS Test")) 
            	softAssert.assertTrue(Chemical_CAS_Input.isDisplayed(), "CAS Test");
            if (Chemical_UNDOT_Input.getAttribute("value").contains("UNDOT Test")) 
               	softAssert.assertTrue(Chemical_UNDOT_Input.isDisplayed(), "UNDOT Test");
           	if (Chemical_Formula_Input.getAttribute("value").contains("Chemical_Formula Test")) 
               	softAssert.assertTrue(Chemical_Formula_Input.isDisplayed(), "Chemical_Formula Test");
           	if (Chemical_Alternate_Name_Input.getAttribute("value").contains("Alternative Name Test")) 
               	softAssert.assertTrue(Chemical_Alternate_Name_Input.isDisplayed(), "Alternative Name Test");
        	  	Chemicals_Window_Particulate_Properties_Tab.click();
        	  	if (Chemimcal_Density_Field.getAttribute("value").contains("22")) 
                   	softAssert.assertTrue(Chemimcal_Density_Field.isDisplayed(), "22");
        	  	if (Chemimcal_Mean_Diamter_Field.getAttribute("value").contains("1")) 
                   	softAssert.assertTrue(Chemimcal_Mean_Diamter_Field.isDisplayed(), "1");
        
        	  	Edit_Chemicals_Window_X.click();
        	  	Chemicals_Window_X.click();
        PageUtil.waitForPageToLoad();
        	}
        	}
        	
    
    
    /*****************************************************
     * Delete Chemical/Administration 
     **********************************************/
    public void Delete_Chemical_Administration() {
    	PageUtil.waitForPageAnimation();
    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();
        
        Actions act = new Actions(driver);
        
        act.moveToElement(Current_Site_Menutext).build().perform();
        if (Current_Site_Menutext.isDisplayed() & Current_Site_Menutext.isEnabled())
        	act.moveToElement(Chemicals_Menutext).build().perform();
        if (Chemicals_Menutext.isDisplayed() & Chemicals_Menutext.isEnabled())
        	Chemicals_Menutext.click();
        	
        	        
        if (Chemicals_Window.isDisplayed() & Chemicals_Window.isEnabled())
        	PageUtil.waitForPageToLoad();
        	Actions act2 = new Actions(driver);
        	act2.doubleClick(getChemicalExist("Test Chemical")).build().perform();
        	PageUtil.waitForPageAnimation();
        	Delete_Chemical_Button.click();
        	PageUtil.waitForPageAnimation();
            Yes_Button.click();
            PageUtil.waitForPageToLoad();

  	       	
    	
        WebElement userRow = getChemicalExist("Test Chemical");
        if(userRow!=null) {
            userRow.click();
          
            PageUtil.waitForPageToLoad();
            softAssert.assertFalse(checkSensorExists("Test Chemical"),
                    "Chemical still exists");
        }
        
        if (Chemicals_Window.getText().contains("Chemicals"))
        	Chemicals_Window_X.click();
        PageUtil.waitForPageToLoad();
    }
        
        
        
            
    
    public WebElement getChemicalExist(String searchKeyword){
        try {
            return driver.findElement(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']"));
        }catch (Exception ex){
            return null;
        }
    }

    public boolean checkChemicalExists(String searchKeyword){
        try {
            return driver.findElements(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']")).size() > 0;
        }catch (Exception ex){
            return false;
        }
    }
    
    
    
    /*****************************************************
     * Add New Met Station/Administration 
     *  @throws AWTException
     **********************************************/
    	public void Add_New_MetStation_Administration() throws AWTException {
        
    	PageUtil.waitForPageToLoad();
    	Dimension dimension = driver.manage().window().getSize();
    	Robot robot = new Robot();
    	robot.delay(100);
    	for(String handle:driver.getWindowHandles()){
    		driver.switchTo().window(handle);
    	}
    	robot.mouseMove(dimension.height/2, dimension.width/2);
    	robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
    	robot.delay(50);
    	robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
    	
    	
    	
    	if (Add_MetStation_Menutext.isDisplayed() & Add_MetStation_Menutext.isEnabled())
    		Add_MetStation_Menutext.click();    	
    	
    		Add_MetStation_Type_Input_Field.click();     
    		Add_MetStation_Type_Value.click();  		
    		Add_MetStation_Name_Input_Field.sendKeys("Met3");
    		Add_MetStation_Add_DAQ_Button.click();
    		
    		PageUtil.waitForPageAnimation();
    		Add_MetStation_DAQ_Name_Field.sendKeys("DAQ3");
    		Add_MetStation_Add_DAQ_Save_Button.click();
	        PageUtil.waitForPageAnimation();
	        Add_MetStation_Add_Port_Button.click();
	        
	        Met_Station_Port_Type_Inputbox.click();
	        Met_Station_Port_Type_Value.click();
	        
	        Met_Station_Port_IP_Input.sendKeys("10.101.101.1");
	        Met_Station_Port_Number_Value.sendKeys("11");
	        Met_Station_Port_Save_Button.click();
	        
	        Met_Station_HeightFT_Inputbox.sendKeys("3");
	        Met_Station_DeviceID_Inputbox.sendKeys("123");
	        Add_MetStation_Encoding_Input_Field.click();
	        Add_MetStation_Encoding_Value.click();
	        Add_MetStation_AddressType_Input_Field.click();
	        Add_MetStation_AdressType_Value.click();
	        Add_MetStation_NumericFormat_Input_Field.click();
	        Add_MetStation_NumericFormat_Value.click();
	        
	        Add_MetStation_Save_Button.click();
	        
	        PageUtil.waitForPageAnimation();
            
	        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
	            Administration_Menutext.click();
	        
	        Actions act4 = new Actions(driver);
	        
	        act4.moveToElement(Data_Source_Menutext).build().perform();
	        if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
	        	act4.moveToElement(DAQ_Menutext).build().perform();	
	        if (Met_Stations_Menutext.isDisplayed() & Met_Stations_Menutext.isEnabled())
	        	Met_Stations_Menutext.click();
            
            
            softAssert.assertTrue(getMetStationExist("ModbusMet3").isDisplayed());
            
            if (Met_Stations_List_Window.getText().contains("Met Stations"))
            	Met_Stations_List_Window_X.click();
            

            
            

            }   
        
    
    
    /*****************************************************
     * Add New Met Station/Administration Form Validation
     * @throws AWTException 
     **********************************************/
    public void Validate_Add_New_MetStation_Administration() throws AWTException {
        
    	PageUtil.waitForPageToLoad();
    	Dimension dimension = driver.manage().window().getSize();
    	Robot robot = new Robot();
    	robot.delay(100);
    	for(String handle:driver.getWindowHandles()){
    		driver.switchTo().window(handle);
    	}
    	robot.mouseMove(dimension.height/2, dimension.width/2);
    	robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
    	robot.delay(50);
    	robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
    	
    	
    	
    	if (Add_MetStation_Menutext.isDisplayed() & Add_MetStation_Menutext.isEnabled())
    		Add_MetStation_Menutext.click();
        	
              	
    		Add_MetStation_Save_Button.click();        	
        	
            softAssert.assertTrue(Add_MetStation_Type_Error.isDisplayed());
            softAssert.assertTrue(Add_MetStation_Name_Error.isDisplayed());
            softAssert.assertTrue(Add_MetStation_Height_Error.isDisplayed());
            
            Add_MetStation_Type_Input_Field.click();     
    		Add_MetStation_Type_Value.click();
            Add_MetStation_Name_Input_Field.sendKeys("Met3");
    		Add_MetStation_Add_DAQ_Button.click();
    		
    		PageUtil.waitForPageAnimation();
    		Add_MetStation_DAQ_Name_Field.sendKeys("ErrorTest3");
    		Add_MetStation_Add_DAQ_Save_Button.click();
	        PageUtil.waitForPageAnimation();
	        Add_MetStation_Add_Port_Button.click();
	        
	        Met_Station_Port_Type_Inputbox.click();
	        Met_Station_Port_Type_Value.click();
	        
	        Met_Station_Port_IP_Input.sendKeys("10.101.101.1");
	        Met_Station_Port_Number_Value.sendKeys("11");
	        Met_Station_Port_Save_Button.click();
	        
	        Add_MetStation_Save_Button.click();   
	        
	        softAssert.assertTrue(Add_MetStation_Height_Error.isDisplayed());
	        softAssert.assertTrue(Add_MetStation_Encoding_Error.isDisplayed());
	        softAssert.assertTrue(Add_MetStation_Adress_Type_Error.isDisplayed());
	        softAssert.assertTrue(Add_MetStation_Numeric_Format_Error.isDisplayed());
	      
            Add_MetStation_Window_X.click();
            
            if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
                Administration_Menutext.click();
            
            Actions act = new Actions(driver);
            
            act.moveToElement(Data_Source_Menutext).build().perform();
            if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
            	act.moveToElement(DAQ_Menutext).build().perform();	
            if (DAQ_Menutext.isDisplayed() & DAQ_Menutext.isEnabled())
            	DAQ_Menutext.click();
                   
            WebElement DAQRow = getDAQExist("ErrorTest3");
            if(DAQRow!=null) {
                DAQRow.click();
                
                Actions action = new Actions(driver);
                action.doubleClick(DAQRow).perform();
                DAQ_Delete_Button.click();
                DAQ_Yes_Button.click();
                
               
                PageUtil.waitForPageAnimation();
                softAssert.assertFalse(checkDAQExists("ErrorTest3"),
                        "DAQ with ErrorTest3 still exists");
            }
            if (DAQ_List_Window.getText().contains("DAQ List"))
            	DAQ_List_Window_X.click();;
            

        }

       
    
    /*****************************************************
     * Edit Met Station/Administration 
     **********************************************/
    	public void Edit_MetStation_Administration()  {
        	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
                Administration_Menutext.click();
    	
        	 Actions act = new Actions(driver);
             
             act.moveToElement(Data_Source_Menutext).build().perform();
             if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
             	act.moveToElement(DAQ_Menutext).build().perform();	
             if (Met_Stations_Menutext.isDisplayed() & Met_Stations_Menutext.isEnabled())
            	 Met_Stations_Menutext.click();
                    
             WebElement DAQRow = getDAQExist("ModbusMet3");
             if(DAQRow!=null) {
                 DAQRow.click();
                 
                 Actions action = new Actions(driver);
                 action.doubleClick(DAQRow).perform();
                 
    	    			
		Add_MetStation_Name_Input_Field.sendKeys("Edited");
        Met_Station_HeightFT_Inputbox.sendKeys("33");
        Met_Station_DeviceID_Inputbox.sendKeys("12345");  
        WebElement webElement = driver.findElement(By.xpath("//label[text()='Enabled']/following-sibling::div/div/div/input[@type='checkbox']"));
        webElement.sendKeys(Keys.TAB);
        webElement.sendKeys(Keys.TAB);
        
        
        Add_MetStation_Save_Button.click();
    	
        PageUtil.waitForPageAnimation();
    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();
        
        Actions act4 = new Actions(driver);
        
        act.moveToElement(Data_Source_Menutext).build().perform();
        if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
        	act4.moveToElement(DAQ_Menutext).build().perform();	
        if (Met_Stations_Menutext.isDisplayed() & Met_Stations_Menutext.isEnabled())
        	Met_Stations_Menutext.click();
             
        softAssert.assertTrue(checkMetStationExists("ModbusMet3Edited"), "Met Station editing was not successful");
        if (Met_Stations_List_Window.getText().contains("Met Stations"))
        	Met_Stations_List_Window_X.click();
             }
    }
    
    /*****************************************************
     * Delete Met Station/Administration 
     **********************************************/
    public void Delete_MetStation_Administration() {
    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();
	
    	 Actions act = new Actions(driver);
         
         act.moveToElement(Data_Source_Menutext).build().perform();
         if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
         	act.moveToElement(DAQ_Menutext).build().perform();	
         if (Met_Stations_Menutext.isDisplayed() & Met_Stations_Menutext.isEnabled())
        	 Met_Stations_Menutext.click();
                
         WebElement MetRow = getDAQExist("ModbusMet3Edited");
         if(MetRow!=null) {
             MetRow.click();
             
             Actions action = new Actions(driver);
             action.doubleClick(MetRow).perform();
             WebElement webElement = driver.findElement(By.xpath("//label[text()='Enabled']/following-sibling::div/div/div/input[@type='checkbox']"));//You can use xpath, ID or name whatever you like
             webElement.sendKeys(Keys.TAB);
             webElement.sendKeys(Keys.TAB);
             Delete_Met_Station_Button.click();
             Met_Station_Yes_Button.click();
             
             PageUtil.waitForPageAnimation();
             if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
                 Administration_Menutext.click();
     	
         	 Actions act9 = new Actions(driver);
              
              act9.moveToElement(Data_Source_Menutext).build().perform();
              if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
              	act9.moveToElement(DAQ_Menutext).build().perform();	
              if (Met_Stations_Menutext.isDisplayed() & Met_Stations_Menutext.isEnabled())
             	 Met_Stations_Menutext.click();
                             
            PageUtil.waitForPageAnimation();
            softAssert.assertFalse(checkMetStationExists("ModbusMet3Edited"),
                    "Met Station with ModbusMet3Edited still exists");
        
        if (Met_Stations_List_Window.getText().contains("Met Stations"))
        	Met_Stations_List_Window_X.click();
    }
         PageUtil.waitForPageAnimation();
         if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();
	
    	 Actions act7 = new Actions(driver);
         
         act7.moveToElement(Data_Source_Menutext).build().perform();
         if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
         	act7.moveToElement(DAQ_Menutext).build().perform();	
         if (DAQ_Menutext.isDisplayed() & DAQ_Menutext.isEnabled())
        	 DAQ_Menutext.click();
                
         WebElement DAQRow = getDAQExist("DAQ3");
         if(DAQRow!=null) {
             DAQRow.click();
             
             Actions action = new Actions(driver);
             action.doubleClick(DAQRow).perform();
             DAQ_Delete_Button.click();
             DAQ_Yes_Button.click();

             PageUtil.waitForPageAnimation();
             softAssert.assertFalse(checkDAQExists("DAQ3"),
                     "DAQ with DAQ3 still exists");
         
    }
    if (DAQ_List_Window.getText().contains("DAQ List"))
    	DAQ_List_Window_X.click();;
         }
    
    
    public WebElement getMetStationExist(String searchKeyword){
        try {
            return driver.findElement(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']"));
        }catch (Exception ex){
            return null;
        }
    }

    public boolean checkMetStationExists(String searchKeyword){
        try {
            return driver.findElements(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']")).size() > 0;
        }catch (Exception ex){
            return false;
        }
    }
    
    
    /*****************************************************
     * Add New Light Device/Administration 
     *  @throws AWTException
     **********************************************/
    	public void Add_New_Light_Device_Administration() throws AWTException {
        
    		PageUtil.waitForPageToLoad();
        	Dimension dimension = driver.manage().window().getSize();
        	Robot robot = new Robot();
        	robot.delay(100);
        	for(String handle:driver.getWindowHandles()){
        		driver.switchTo().window(handle);
        	}
        	robot.mouseMove(dimension.height/2, dimension.width/2);
        	robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        	robot.delay(50);
        	robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
        	
        	
        	
        	if (Add_Sensor_Menutext.isDisplayed() & Add_Sensor_Menutext.isEnabled())
        		Add_Sensor_Menutext.click();
        	
        	Actions act = new Actions(driver);
            
            act.moveToElement(Sensor_Type_Menutext).build().perform();
            if (Sensor_Type_Menutext.isDisplayed() & Sensor_Type_Menutext.isEnabled())
            	act.moveToElement(Sensor_Type_Menutext).build().perform();	
            if (Sensor_Type_Menutext.isDisplayed() & Sensor_Type_Menutext.isEnabled())
            	Sensor_Type_Menutext.click();
            	
            
            PageUtil.waitForPageToLoad();
        	Dimension dimension2 = driver.manage().window().getSize();
        	Robot robot2 = new Robot();
        	robot2.delay(100);
        	for(String handle:driver.getWindowHandles()){
        		driver.switchTo().window(handle);
        	}
        	robot2.mouseMove(dimension2.height/2, dimension.width/2);
        	robot2.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        	robot2.delay(50);
        	robot2.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
    	
        	Add_Light_Sensor_Name_Input.sendKeys("Light Sensor1");
        	Add_Light_Sensor_Type_Input_Field.click();
        	Add_Light_Sensor_Type_Input_Value.click();
        	Add_Light_Sensor_Interface_Input_Field.click();
        	Add_Light_Sensor_Interface_Input_Value.click();
        	
        	
        	Actions actions = new Actions(driver);
        	actions.moveToElement(Add_Light_Sensor_Add_Sensor_Button);
        	actions.perform();
        	Add_Light_Sensor_Add_Sensor_Button.click();
        	
        	PageUtil.waitForPageAnimation();
        	
	    	if (Add_Light_Sensor_Edit_Sensor_Window.getText().contains("Edit Sensor"))
	    		Add_Light_Sensor_Edit_Sensor_Chemical_Input_Field.click();
	    	Add_Light_Sensor_Edit_Sensor_Chemical_Input_Field.sendKeys(Keys.DOWN);
	    	Add_Light_Sensor_Edit_Sensor_Chemical_Input_Field_Value.click();
	    	Add_Light_Sensor_Edit_Sensor_Channel_Tag_Input.sendKeys("12345");
	    	Add_Light_Sensor_Edit_Sensor_Over_Range.sendKeys("12345");
	    	Add_Light_Sensor_Edit_SEnsor_Save_Button.click();
	    		    	
	    	Actions actions2 = new Actions(driver);
	    	actions2.moveToElement(Sensor_Save_Button);
	    	actions2.perform();
	    	
	    	Sensor_Save_Button.click();
        	    	
        	PageUtil.waitForPageAnimation();
            
	        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
	            Administration_Menutext.click();
	        
	        Actions act4 = new Actions(driver);
	        
	        act4.moveToElement(Data_Source_Menutext).build().perform();
	        if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
	        	act4.moveToElement(DAQ_Menutext).build().perform();	
	        if (Light_Devices_Menutext.isDisplayed() & Light_Devices_Menutext.isEnabled())
	        	Light_Devices_Menutext.click();
            
            
            softAssert.assertTrue(getLightDeviceExist("Light Sensor1").isDisplayed());
            
            if (Light_Devices_List_Window.getText().contains("Light Devices"))
            	Light_Devices_List_Window_X.click();
            

            
            

            }   
        
    
    
    /*****************************************************
     * Add New Light Device/Administration Form Validation
     * @throws AWTException 
     **********************************************/
    public void Validate_Add_New_Light_Device_Administration() throws AWTException {
        
    	PageUtil.waitForPageToLoad();
    	Dimension dimension = driver.manage().window().getSize();
    	Robot robot = new Robot();
    	robot.delay(100);
    	for(String handle:driver.getWindowHandles()){
    		driver.switchTo().window(handle);
    	}
    	robot.mouseMove(dimension.height/2, dimension.width/2);
    	robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
    	robot.delay(50);
    	robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
    	
    	
    	
    	if (Add_Sensor_Menutext.isDisplayed() & Add_Sensor_Menutext.isEnabled())
    		Add_Sensor_Menutext.click();
    	
    	Actions act = new Actions(driver);
        
        act.moveToElement(Sensor_Type_Menutext).build().perform();
        if (Sensor_Type_Menutext.isDisplayed() & Sensor_Type_Menutext.isEnabled())
        	act.moveToElement(Sensor_Type_Menutext).build().perform();	
        if (Sensor_Type_Menutext.isDisplayed() & Sensor_Type_Menutext.isEnabled())
        	Sensor_Type_Menutext.click();
        	
        
        PageUtil.waitForPageToLoad();
    	Dimension dimension2 = driver.manage().window().getSize();
    	Robot robot2 = new Robot();
    	robot2.delay(100);
    	for(String handle:driver.getWindowHandles()){
    		driver.switchTo().window(handle);
    	}
    	robot2.mouseMove(dimension2.height/2, dimension.width/2);
    	robot2.mousePress(InputEvent.BUTTON1_DOWN_MASK);
    	robot2.delay(50);
    	robot2.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                	
    	
    	Actions actions = new Actions(driver);
    	actions.moveToElement(Sensor_Save_Button);
    	actions.perform();

    	Sensor_Save_Button.click();
    	        	
        softAssert.assertTrue(Add_Light_Sensor_Name_Error.isDisplayed());
        softAssert.assertTrue(Add_Light_Sensor_Type_Error.isDisplayed());
        softAssert.assertTrue(Add_Light_Sensor_Required_Error.isDisplayed());
        
       
        Actions actions2 = new Actions(driver);
    	actions2.sendKeys(Keys.ESCAPE).perform();
        
            
                       

        }

       
    
    /*****************************************************
     * Edit Light Device/Administration 
     **********************************************/
    	public void Edit_Light_Device_Administration()  {
        	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
                Administration_Menutext.click();
    	
        	 Actions act = new Actions(driver);
             
             act.moveToElement(Data_Source_Menutext).build().perform();
             if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
             	act.moveToElement(Light_Devices_Menutext).build().perform();	
             if (Light_Devices_Menutext.isDisplayed() & Light_Devices_Menutext.isEnabled())
            	 Light_Devices_Menutext.click();
                    
             WebElement DAQRow = getDAQExist("Light Sensor1");
             if(DAQRow!=null) {
                 DAQRow.click();
                 
                 Actions action = new Actions(driver);
                 action.doubleClick(DAQRow).perform();
                 
    	    			
        Add_Light_Sensor_Name_Input.sendKeys("Edited");
        Actions actions2 = new Actions(driver);
    	actions2.moveToElement(Sensor_Save_Button);
    	actions2.perform();
    	Sensor_Save_Button.click();
           	             
        softAssert.assertTrue(checkLightDeviceExists("Light Sensor1Edited"), "Light Sensor editing was not successful");
        if (Light_Devices_List_Window.getText().contains("Light Devices"))
        	Light_Devices_List_Window_X.click();
             }
    }
    
    /*****************************************************
     * Delete Light Device/Administration 
     **********************************************/
    public void Delete_Light_Device_Administration() {
    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            Administration_Menutext.click();
	
    	 Actions act = new Actions(driver);
         
         act.moveToElement(Data_Source_Menutext).build().perform();
         if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
         	act.moveToElement(DAQ_Menutext).build().perform();	
         if (Light_Devices_Menutext.isDisplayed() & Light_Devices_Menutext.isEnabled())
        	 Light_Devices_Menutext.click();
                
         WebElement DeviceRow = getLightDeviceExist("Light Sensor1Edited");
         if(DeviceRow!=null) {
             DeviceRow.click();
             
            Actions action = new Actions(driver);
            action.doubleClick(DeviceRow).perform();
            PageUtil.waitForPageAnimation();

            Actions actions2 = new Actions(driver);
         	actions2.moveToElement(Light_Devices_Delete_Button);
         	actions2.perform();
             Light_Devices_Delete_Button.click();
             Light_Devices_Delete_Yes_Button.click();
             
                          
                                          
            PageUtil.waitForPageAnimation();
            
            softAssert.assertFalse(checkLightDeviceExists("Light Sensor1Edited"),
                    "Light Sensor with Light Sensor1 Edited still exists");
        
        if (Light_Devices_List_Window.getText().contains("Light Devices"))
        	Light_Devices_List_Window_X.click();
    }
        
    }
    
    public WebElement getLightDeviceExist(String searchKeyword){
        try {
            return driver.findElement(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']"));
        }catch (Exception ex){
            return null;
        }
    }

    public boolean checkLightDeviceExists(String searchKeyword){
        try {
            return driver.findElements(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']")).size() > 0;
        }catch (Exception ex){
            return false;
        }
    }
    
    

    /*****************************************************
     * Add New RAE Sensor/Administration 
     *  @throws AWTException
     **********************************************/
    	public void Add_New_RAE_Sensor_Administration() throws AWTException {
        
    	PageUtil.waitForPageToLoad();
    	Dimension dimension = driver.manage().window().getSize();
    	Robot robot = new Robot();
    	robot.delay(100);
    	for(String handle:driver.getWindowHandles()){
    		driver.switchTo().window(handle);
    	}
    	robot.mouseMove(dimension.height/2, dimension.width/2);
    	robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
    	robot.delay(50);
    	robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
    	
    	
    	if (Add_Sensor_Menutext.isDisplayed() & Add_Sensor_Menutext.isEnabled())
    		Add_Sensor_Menutext.click();
    	
    	Actions act = new Actions(driver);
        
        act.moveToElement(Add_RAE_Sensor_Menutext).build().perform();
        if (Add_RAE_Sensor_Menutext.isDisplayed() & Add_RAE_Sensor_Menutext.isEnabled())
        	act.moveToElement(Sensor__RAE_Type_Menutext).build().perform();	
        if (Sensor__RAE_Type_Menutext.isDisplayed() & Sensor__RAE_Type_Menutext.isEnabled())
        	Sensor__RAE_Type_Menutext.click();
    	 		
        	Add_RAE_Sensor_Name_Input_Field.sendKeys("RAE3Test");
        	Add_RAE_Sensor_Add_DAQ_Button.click();
    		
    		PageUtil.waitForPageAnimation();
    		Add_MetStation_DAQ_Name_Field.sendKeys("DAQ3");
    		Add_MetStation_Add_DAQ_Save_Button.click();
	        PageUtil.waitForPageAnimation();
	        Add_RAE_Sensor_Add_Port_Button.click();
	        
	        Met_Station_Port_Type_Inputbox.click();
	        Met_Station_Port_Type_Value.click();
	        
	        Met_Station_Port_IP_Input.sendKeys("10.101.101.1");
	        Met_Station_Port_Number_Value.sendKeys("11");
	        Met_Station_Port_Save_Button.click();
	        PageUtil.waitForPageAnimation();
	       	        
	        Sensor_RAE_Save_Button.click();
	        
	        
	        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
	            Administration_Menutext.click();
		
	    	 Actions act7 = new Actions(driver);
	         
	         act7.moveToElement(Data_Source_Menutext).build().perform();
	         if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
	         	act7.moveToElement(DAQ_Menutext).build().perform();	
	         if (DAQ_Menutext.isDisplayed() & DAQ_Menutext.isEnabled())
	        	 DAQ_Menutext.click();
	                
	         WebElement DAQRow = getDAQExist("DAQ3");
	         if(DAQRow!=null) {
	             DAQRow.click();
	             
	             Actions action = new Actions(driver);
	             action.doubleClick(DAQRow).perform();
	             DAQ_Delete_Button.click();
	             DAQ_Yes_Button.click();

	             PageUtil.waitForPageAnimation();
	             softAssert.assertFalse(checkRAESensorExists("DAQ3"),
	                     "DAQ with DAQ3 still exists");
	         
	    }
	    if (DAQ_List_Window.getText().contains("DAQ List"))
	    	DAQ_List_Window_X.click();;
	         
	    	 
	    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
	        	Administration_Menutext.click(); 
	        if (Data_Source_Menutext.isDisplayed() & Data_Source_Menutext.isEnabled())
	        	act.moveToElement(Data_Source_Menutext).build().perform();
	        if (Sensor_Interfaces_Menutext.isDisplayed() & Sensor_Interfaces_Menutext.isEnabled())
	        	act.moveToElement(Sensor_Interfaces_Menutext).build().perform();
	        	Sensor_Interfaces_Menutext.click();
	    	Actions act2 = new Actions(driver);
	        act2.click(getRAESEnsorExist("RAERAE3Test")).build().perform();
	         Delete_Sensor_Button.click();
	         Yes_Button.click();
	         PageUtil.waitForPageToLoad();
	         	
	     	
	         WebElement userRow = getRAESEnsorExist("RAERAE3Test");
	         if(userRow!=null) {
	             userRow.click();
	           
	             PageUtil.waitForPageToLoad();
	             softAssert.assertFalse(checkSensorExists("RAERAE3Test"),
	                     "Sensor  still exists");
	         }
	         
	         if (Sensor_Interfaces_List_Window.getText().contains("Sensor Interfaces"))
	         	Sensor_Interfaces_List_Window_X.click();
   
    	}
    	
    	
	        
    	public WebElement getRAESEnsorExist(String searchKeyword){
            try {
                return driver.findElement(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']"));
            }catch (Exception ex){
                return null;
            }
        }
    	
        public boolean checkRAESensorExists(String searchKeyword){
            try {
                return driver.findElements(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']")).size() > 0;
            }catch (Exception ex){
                return false;
            }
            

            }   
        
    
    
    /*****************************************************
     * Add New RAE Sensor/Administration Form Validation
     * @throws AWTException 
     **********************************************/
    public void Validate_Add_New_RAE_Sensor_Administration() throws AWTException {
        
    	PageUtil.waitForPageToLoad();
    	Dimension dimension = driver.manage().window().getSize();
    	Robot robot = new Robot();
    	robot.delay(100);
    	for(String handle:driver.getWindowHandles()){
    		driver.switchTo().window(handle);
    	}
    	robot.mouseMove(dimension.height/2, dimension.width/2);
    	robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
    	robot.delay(50);
    	robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
    	
    	
    	
    	if (Add_Sensor_Menutext.isDisplayed() & Add_Sensor_Menutext.isEnabled())
    		Add_Sensor_Menutext.click();
    	
    	Actions act = new Actions(driver);
        
        act.moveToElement(Add_RAE_Sensor_Menutext).build().perform();
        if (Add_RAE_Sensor_Menutext.isDisplayed() & Add_RAE_Sensor_Menutext.isEnabled())
        	act.moveToElement(Sensor__RAE_Type_Menutext).build().perform();	
        if (Sensor__RAE_Type_Menutext.isDisplayed() & Sensor__RAE_Type_Menutext.isEnabled())
        	Sensor__RAE_Type_Menutext.click();
            	
        Sensor_RAE_Save_Button.click();        	
        	
            softAssert.assertTrue(Add_RAE_Sensor_DAQ_Error.isDisplayed());
            softAssert.assertTrue(Add_RAE_Sensor_Port_Error.isDisplayed());
            
            
           
            Add_RAE_Sensor_Window_X.click();
            
                       

        }
    
    /*****************************************************
     * Create Report
     **********************************************/
    
    public void testCreatingReport() throws InterruptedException {
        if (GoTomenu_Text.isDisplayed() && GoTomenu_Text.isEnabled())
            GoTomenu_Text.click();
        if (QRCreatereport_Button.isDisplayed() && QRCreatereport_Button.isEnabled()){
            QRCreatereport_Button.click();
        }
        if (createreport_Button.isDisplayed() && createreport_Button.isEnabled()){
            createreport_Button.click();
        }

        String myHomePath= System.getProperty("user.home");
        File folder = new File(myHomePath + "/Downloads/");
        File[] listOfFiles = folder.listFiles();

        Thread.sleep(5000);
        softAssert.assertTrue(isReportCreatedInLast1hour(listOfFiles));
    }
    
    /*****************************************************
     * Event History
     **********************************************/

    public void testEventHistory() throws InterruptedException {
        if (GoTomenu_Text.isDisplayed() && GoTomenu_Text.isEnabled())
            GoTomenu_Text.click();

        if (QREventHistory_Button.isDisplayed() && QREventHistory_Button.isEnabled()){
            QREventHistory_Button.click();
        }
        Actions action = new Actions(driver);
        action.moveToElement(Gas_Release_Event).doubleClick().build().perform();

        softAssert.assertTrue(Gas_Release_Report.isDisplayed());
    }
    
    /*****************************************************
     * Met Report
     **********************************************/

    public void testMetReport() throws InterruptedException {
        if (GoTomenu_Text.isDisplayed() && GoTomenu_Text.isEnabled())
            GoTomenu_Text.click();
        if (MetHistorymenu_Text.isDisplayed() && MetHistorymenu_Text.isEnabled()){
            MetHistorymenu_Text.click();
        }
        softAssert.assertTrue(MetHistory_Report.isDisplayed());
    }

    boolean isReportCreatedInLast1hour(File[] listOfFiles){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, -1);
        long last1Hour = cal.getTimeInMillis();
        for(File file: listOfFiles){
            if(file.lastModified()>last1Hour){
                return true;
            }
        }
        return false;
    }
    
    /*****************************************************
     * Current Site Settings
     **********************************************/
    public void Current_Site_Settings_Administration() {
        if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
        	Administration_Menutext.click(); 
        Actions act = new Actions(driver);
        if (Current_Site_Menutext.isDisplayed() & Current_Site_Menutext.isEnabled())
        	act.moveToElement(Current_Site_Menutext).build().perform();
        if (Settings_Menutext.isDisplayed() & Settings_Menutext.isEnabled())
        	act.moveToElement(Settings_Menutext).build().perform();
        	Settings_Menutext.click();
        if (Settings_Window.getText().contains("Settings"))
        	Settings_Window_Wind_Speed_Input_Field.clear();
        	Settings_Window_Wind_Speed_Input_Field.sendKeys("23");
        	Settings_Window_High_Temp_Input_Field.clear();
        	Settings_Window_High_Temp_Input_Field.sendKeys("34");
        	Settings_Window_Low_Temp_Input_Field.clear();
        	Settings_Window_Low_Temp_Input_Field.sendKeys("23");
        	Settings_Window_Map_Width_Input_Field.clear();
        	Settings_Window_Map_Width_Input_Field.sendKeys("664");
        	Settings_Window_ERG_Data_Input_Field.clear();
        	Settings_Window_ERG_Data_Input_Field.sendKeys("20");
        	Settings_Window_Interval_Corridor_Input_Field.clear();
        	Settings_Window_Interval_Corridor_Input_Field.sendKeys("31");
        	Settings_Window_Earth_Netwrork_Key_Input_Field.clear();
        	Settings_Window_Earth_Netwrork_Key_Input_Field.sendKeys("1");
        	Settings_Window_Max_Zoom_Level_Input_Field.clear();
        	Settings_Window_Max_Zoom_Level_Input_Field.sendKeys("20");
        	Settings_Window_Zoom_Level_Places_Input_Field.clear();
        	Settings_Window_Zoom_Level_Places_Input_Field.sendKeys("13");
        	Settings_Window_Zoom_Level_Sensors_Input_Field.clear();
        	Settings_Window_Zoom_Level_Sensors_Input_Field.sendKeys("13");
        	Settings_Window_Limits_Tab.click();
        	Settings_Window_Low_Psi_Input_Field.clear();
        	Settings_Window_Low_Psi_Input_Field.sendKeys("0.16");
        	Settings_Description1_Input_Field.clear();
        	Settings_Description1_Input_Field.sendKeys("Low PSI Descripion");
        	Settings_Window_Medium_Psi_Input_Field.clear();
        	Settings_Window_Medium_Psi_Input_Field.sendKeys("3.00");
        	Settings_Description2_Input_Field.clear();
        	Settings_Description2_Input_Field.sendKeys("Medium PSI Descripion");
        	Settings_Window_High_Psi_Input_Field.clear();
        	Settings_Window_High_Psi_Input_Field.sendKeys("8.00");
        	Settings_Description3_Input_Field.clear();
        	Settings_Description3_Input_Field.sendKeys("High PSI Descripion");
        	Settings_Window_Thermal_Limits_Low_Input_Field.clear();
        	Settings_Window_Thermal_Limits_Low_Input_Field.sendKeys("1700");
        	Settings_Description4_Input_Field.clear();
        	Settings_Description4_Input_Field.sendKeys("Thermal Low Descripion");
        	Settings_Window_Thermal_Limits_Medium_Input_Field.clear();
        	Settings_Window_Thermal_Limits_Medium_Input_Field.sendKeys("5000");
        	Settings_Description5_Input_Field.clear();
        	Settings_Description5_Input_Field.sendKeys("Thermal Medium Descripion");
        	Settings_Window_Thermal_Limits_High_Input_Field.clear();
        	Settings_Window_Thermal_Limits_High_Input_Field.sendKeys("8000");
        	Settings_Description6_Input_Field.clear();
        	Settings_Description6_Input_Field.sendKeys("Thermal High Descripion");
        	Settings_Window_License_Tab.click();
        	Settings_Window_License_Lightning_Checkbox.click();
        	
        	
        	Settings_Save_Button.click();
    }
        	
        	/*****************************************************
             * Current Site Settings Edit Validation
             **********************************************/
            public void Current_Site_Settings_Validation_Administration() {
                if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
                	Administration_Menutext.click(); 
                Actions act = new Actions(driver);
                if (Current_Site_Menutext.isDisplayed() & Current_Site_Menutext.isEnabled())
                	act.moveToElement(Current_Site_Menutext).build().perform();
                if (Settings_Menutext.isDisplayed() & Settings_Menutext.isEnabled())
                	act.moveToElement(Settings_Menutext).build().perform();
                	Settings_Menutext.click();
                if (Settings_Window.getText().contains("Settings")){
                	
                	
                	String value = Settings_Window_Wind_Speed_Input_Field.getAttribute("innerHtml");
                	if (Settings_Window_Wind_Speed_Input_Field.getAttribute("value").contains("23")) {
                		softAssert.assertTrue(Settings_Window_Wind_Speed_Input_Field.isDisplayed(), "21");
                		if (Settings_Window_High_Temp_Input_Field.getAttribute("value").contains("34")) 
                    		softAssert.assertTrue(Settings_Window_High_Temp_Input_Field.isDisplayed(), "34");
                		if (Settings_Window_Low_Temp_Input_Field.getAttribute("value").contains("23")) 
                    		softAssert.assertTrue(Settings_Window_Low_Temp_Input_Field.isDisplayed(), "23");
                		if (Settings_Window_Map_Width_Input_Field.getAttribute("value").contains("664")) 
                    		softAssert.assertTrue(Settings_Window_Map_Width_Input_Field.isDisplayed(), "664");
                		if (Settings_Window_ERG_Data_Input_Field.getAttribute("value").contains("20")) 
                    		softAssert.assertTrue(Settings_Window_ERG_Data_Input_Field.isDisplayed(), "20");
                		if (Settings_Window_Interval_Corridor_Input_Field.getAttribute("value").contains("31")) 
                    		softAssert.assertTrue(Settings_Window_Interval_Corridor_Input_Field.isDisplayed(), "31");
                		if (Settings_Window_Earth_Netwrork_Key_Input_Field.getAttribute("value").contains("1")) 
                    		softAssert.assertTrue(Settings_Window_Earth_Netwrork_Key_Input_Field.isDisplayed(), "1");
                		if (Settings_Window_Max_Zoom_Level_Input_Field.getAttribute("value").contains("20")) 
                    		softAssert.assertTrue(Settings_Window_Max_Zoom_Level_Input_Field.isDisplayed(), "20");
                		if (Settings_Window_Zoom_Level_Places_Input_Field.getAttribute("value").contains("13")) 
                    		softAssert.assertTrue(Settings_Window_Zoom_Level_Places_Input_Field.isDisplayed(), "13");
                		if (Settings_Window_Zoom_Level_Sensors_Input_Field.getAttribute("value").contains("13")) 
                    		softAssert.assertTrue(Settings_Window_Zoom_Level_Sensors_Input_Field.isDisplayed(), "13");
                		Settings_Window_Limits_Tab.click();
                		if (Settings_Window_Low_Psi_Input_Field.getAttribute("value").contains("0.16")) 
                    		softAssert.assertTrue(Settings_Window_Low_Psi_Input_Field.isDisplayed(), "0.16");
                		if (Settings_Description1_Input_Field.getAttribute("value").contains("Low PSI Descripion")) 
                    		softAssert.assertTrue(Settings_Description1_Input_Field.isDisplayed(), "Low PSI Descripion");
                		if (Settings_Window_Medium_Psi_Input_Field.getAttribute("value").contains("3.00")) 
                    		softAssert.assertTrue(Settings_Window_Medium_Psi_Input_Field.isDisplayed(), "3.00");
                		if (Settings_Description2_Input_Field.getAttribute("value").contains("Medium PSI Descripion")) 
                    		softAssert.assertTrue(Settings_Description2_Input_Field.isDisplayed(), "Medium PSI Descripion");
                		if (Settings_Window_High_Psi_Input_Field.getAttribute("value").contains("8.00")) 
                    		softAssert.assertTrue(Settings_Window_High_Psi_Input_Field.isDisplayed(), "8.00");
                		if (Settings_Description3_Input_Field.getAttribute("value").contains("High PSI Descripion")) 
                    		softAssert.assertTrue(Settings_Description3_Input_Field.isDisplayed(), "High PSI Descripion");
                		if (Settings_Window_Thermal_Limits_Low_Input_Field.getAttribute("value").contains("1700")) 
                    		softAssert.assertTrue(Settings_Window_Thermal_Limits_Low_Input_Field.isDisplayed(), "1700");
                		if (Settings_Description4_Input_Field.getAttribute("value").contains("Thermal Low Descripion")) 
                    		softAssert.assertTrue(Settings_Description4_Input_Field.isDisplayed(), "Thermal Low Descripion");
                		if (Settings_Window_Thermal_Limits_Medium_Input_Field.getAttribute("value").contains("5000")) 
                    		softAssert.assertTrue(Settings_Window_Thermal_Limits_Medium_Input_Field.isDisplayed(), "5000");
                		if (Settings_Description5_Input_Field.getAttribute("value").contains("Thermal Medium Descripion")) 
                    		softAssert.assertTrue(Settings_Description5_Input_Field.isDisplayed(), "Thermal Medium Descripion");
                		if (Settings_Window_Thermal_Limits_High_Input_Field.getAttribute("value").contains("8000")) 
                    		softAssert.assertTrue(Settings_Window_Thermal_Limits_High_Input_Field.isDisplayed(), "8000");
                		if (Settings_Description6_Input_Field.getAttribute("value").contains("Thermal High Descripion")) 
                    		softAssert.assertTrue(Settings_Description6_Input_Field.isDisplayed(), "Thermal High Descripion");
                		
        }   
    }
 
     }   

            /*****************************************************
             * Add New Scenario/Administration 
             **********************************************/
            public void Add_New_Scenario_Administration() {
                
            	    	PageUtil.waitForPageAnimation();
            	    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
            	            Administration_Menutext.click();
            	        
            	        Actions act = new Actions(driver);
            	        
            	        act.moveToElement(Current_Site_Menutext).build().perform();
            	        if (Current_Site_Menutext.isDisplayed() & Current_Site_Menutext.isEnabled())
            	        	act.moveToElement(Scenarios_Menutext).build().perform();
            	        if (Scenarios_Menutext.isDisplayed() & Scenarios_Menutext.isEnabled())
            	        	Scenarios_Menutext.click();
            	        	
            	        	        
            	        if (Scenarios_Window.isDisplayed() & Scenarios_Window.isEnabled())
            	        	PageUtil.waitForPageToLoad();
            	        
            	        Scenarios_Add_Button.click();
            	        act.moveToElement(Scenario_Add_Value).build().perform();
            	        if (Scenario_Add_Value.isDisplayed() & Scenario_Add_Value.isEnabled())
            	        	Scenario_Add_Value.click();
            	        Chemimcal_Liquid_Area_Name_Input.sendKeys("Test Scenario");
            	        Chemimcal_Liquid_Release_Chemical_Field.click();
            	        Chemimcal_Liquid_Release_Chemical_Value.click();
            	        Chemimcal_Liquid_Release_Rate_Input.sendKeys("11");           	        	
            	        	    	        	
            	        Chemimcal_Liquid_Release_Save_Button.click();
            	        	
            	        	PageUtil.waitForPageAnimation();
                                
                    
                    softAssert.assertTrue(getScenarioExist("Test Scenario").isDisplayed());
                    
                   
                    Actions actions2 = new Actions(driver);
                	actions2.sendKeys(Keys.ESCAPE).perform();

                    }               	
            
            
            /*****************************************************
             * Delete Scenario/Administration 
             **********************************************/
            public void Delete_Scenario_Administration() {
            	PageUtil.waitForPageAnimation();
    	    	if (Administration_Menutext.isDisplayed() & Administration_Menutext.isEnabled())
    	            Administration_Menutext.click();
    	        
    	        Actions act = new Actions(driver);
    	        
    	        act.moveToElement(Current_Site_Menutext).build().perform();
    	        if (Current_Site_Menutext.isDisplayed() & Current_Site_Menutext.isEnabled())
    	        	act.moveToElement(Scenarios_Menutext).build().perform();
    	        if (Scenarios_Menutext.isDisplayed() & Scenarios_Menutext.isEnabled())
    	        	Scenarios_Menutext.click();
    	        	
    	        	        
    	        if (Scenarios_Window.isDisplayed() & Scenarios_Window.isEnabled())
    	        	PageUtil.waitForPageToLoad();
                
                	Actions act2 = new Actions(driver);
                	act2.click(getScenarioExist("Test Scenario")).build().perform();
                	PageUtil.waitForPageAnimation();
                	Predefined_Scenarios_Delete_Button.click();
                	PageUtil.waitForPageAnimation();
                    Yes_Button.click();
                    PageUtil.waitForPageToLoad();

          	       	
            	
                WebElement userRow = getScenarioExist("Test Scenario");
                if(userRow!=null) {
                    userRow.click();
                  
                    PageUtil.waitForPageToLoad();
                    softAssert.assertFalse(checkScenarioExists("Test Scenario"),
                            "Scenario still exists");
                }
                
                
                Actions actions2 = new Actions(driver);
            	actions2.sendKeys(Keys.ESCAPE).perform();
            }
                
                
                
                    
            
            public WebElement getScenarioExist(String searchKeyword){
                try {
                    return driver.findElement(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']"));
                }catch (Exception ex){
                    return null;
                }
            }

            public boolean checkScenarioExists(String searchKeyword){
                try {
                    return driver.findElements(By.xpath("//div[contains(@class,'Css3GridAppearance-GridStyle-cellInner') and text()='" + searchKeyword + "']")).size() > 0;
                }catch (Exception ex){
                    return false;
                }
            }
            
            
            /*****************************************************
             * Add New Place
             *  @throws AWTException
             **********************************************/
            	public void Add_Delete_Place_Administration() throws AWTException {
                
            	PageUtil.waitForPageToLoad();
            	Dimension dimension = driver.manage().window().getSize();
            	Robot robot = new Robot();
            	robot.delay(100);
            	for(String handle:driver.getWindowHandles()){
            		driver.switchTo().window(handle);
            	}
            	robot.mouseMove(dimension.height/2, dimension.width/2);
            	robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
            	robot.delay(50);
            	robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
            	
            	
            	if (Add_Place_Menutext.isDisplayed() & Add_Place_Menutext.isEnabled())
            		Add_Place_Menutext.click();
            	
            	            	 		
            	Place_Name_Input.sendKeys("DELETETHISPLACE21");
            	Place_Type_Input_Field.click();
            	Place_Type_Input_Value.click();
            	Place_Save_Button.click();
           		PageUtil.waitForPageAnimation();
           		
           		Place_On_Map.isDisplayed();
           		Actions action = new Actions(driver).contextClick(Place_On_Map);
           		action.build().perform();
           	 Actions act = new Actions(driver);
 	        
 	        act.moveToElement(Edit_Place_Menu).build().perform();
 	       if (Edit_Place_Menu.isDisplayed() & Edit_Place_Menu.isEnabled())
 	    	  Edit_Place_Menu.click();
 	       
 	      Place_Delete_Button.click();
 	      Yes_Button.click();
 	     softAssert.assertTrue(Place_On_Map.isDisplayed(), "PLACE WAS DELETED SUCCESS");               		
        	      
            	}
            	
            	
            	 /*****************************************************
                 * Add Delete Emission Source
                 *  @throws AWTException
                 **********************************************/
                	public void Add_Delete_Emission_Source_Administration() throws AWTException {
                    
                	PageUtil.waitForPageToLoad();
                	Dimension dimension = driver.manage().window().getSize();
                	Robot robot = new Robot();
                	robot.delay(100);
                	for(String handle:driver.getWindowHandles()){
                		driver.switchTo().window(handle);
                	}
                	robot.mouseMove(dimension.height/2, dimension.width/2);
                	robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
                	robot.delay(50);
                	robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
                	
                	
                	if (Emisison_Source_Menutext.isDisplayed() & Emisison_Source_Menutext.isEnabled())
                		Emisison_Source_Menutext.click();
                	
                	Actions actions = new Actions(driver);
        	    	actions.moveToElement(Emisison_Gas_Source_Menutext);
        	    	actions.perform();
        	    	
        	    	Emisison_Gas_Source_Menutext.click();
                	
                	            	 		
        	    	Gas_Source_Area_Name_Input.sendKeys("Gas Source Test");
        	    	PageUtil.waitForPageAnimation();
        	    	Add_Emisison_Source_Chemical_Input_Field.click();
        	    	PageUtil.waitForPageAnimation();
        	    	Add_Emission_Source_Chemical_Input_Field_Value.click();
        	    	Emission_Save_Button.click();
               		PageUtil.waitForPageAnimation();
               		
               		Gas_Emisison_On_Map.isDisplayed();
               		Actions action = new Actions(driver).contextClick(Gas_Emisison_On_Map);
               		action.build().perform();
               	 Actions act = new Actions(driver);
     	        
     	        act.moveToElement(Edit_Place_Menu).build().perform();
     	       if (Edit_Place_Menu.isDisplayed() & Edit_Place_Menu.isEnabled())
     	    	  Edit_Place_Menu.click();
     	       
     	      Emission_Delete_Button.click();
     	     Emission_Source_Delete_Yes_Button.click();
     	     softAssert.assertTrue(Gas_Emisison_On_Map.isDisplayed(), "EMISSION WAS DELETED SUCCESS");               		
            	      
                	}
}
