package com.screeninglog.objLib;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ScreeningLogObjLib {
	
    // ************************************************
    //            Constants
    // ************************************************
    public static final long constant_MaxTimeoutPeriod = 60000;
    
    
	
    // ************************************************
    //            Environment TEST DRIVER
    // ************************************************	
    // Change environment below:
    public static String environment;
    public static String pageUrl;
    public static String URLStartString;
    public static String URLAddress;
    public static String URLLoginString;
    public static String username;
    public static String password;
    public static String basicusername;
    public static String basicuserpassword;
    


    static {
        // ************************************************
        //            QA Settings
        // ************************************************

         String QA_username = "SVC-QA-ADMIN-AUTOMATION@imax.com";
         String QA_password = "ivq6ipcum$33miuu";
         String QA_Basicusername = "SVC-QA-AUTOMATION@imax.com";
         String QA_Basicuserpassword = "!t3fzaxtabast#83";
       


        // ************************************************
        //            UAT Settings
        // ************************************************

         String uat_username = "GSimkin@imax.com";
         String uat_password = "Secret_1387";

         environment = System.getProperty("environment");

        if (ScreeningLogObjLib.environment.equalsIgnoreCase("qa")) {
        	URLStartString = "https:";
        	URLAddress = "screeninglog.qa.imax.com/sl/";
            username = QA_username;
            password = QA_password;
            basicusername = QA_Basicusername;
            basicuserpassword = QA_Basicuserpassword;

            
            pageUrl = URLStartString + URLAddress;
            
        } else if (ScreeningLogObjLib.environment.equalsIgnoreCase("uat")) {
        	URLStartString = "https:";
        	URLAddress = "uat-screeninglog.imax.com/sl/#/trailerview";
            username = uat_username;
            password = uat_password;
            
            pageUrl = URLStartString + URLAddress ;
        }

    }



    
    
    
    // ************************************************
    //            Login Screen
    // ************************************************
    
    
    
    
    
    
    @FindBy(xpath = "//*[@id='x-auto-1-input']")
    public static WebElement Email_Input_Field;

    @FindBy(xpath = "//*[@id='x-auto-0-input']")
    public static WebElement Password_Input_Field;

    @FindBy(xpath = "//*[@id='x-auto-2']")
    public static WebElement RememberMe_Checkbox;

    @FindBy(xpath = "//*[text()='Log In']")
	public static WebElement Signin_Button;

    @FindBy(xpath = "//*[text()='Forgot Password']")
    public static WebElement Forgot_Password_Link;

    @FindBy(linkText = "Forgot Password")
    public static WebElement Forgot_Password_Link_one;

    @FindBy(xpath = "//*[@id='x-auto-1']/img")
    public static WebElement Email_RequiredField_Validation_Sign;

    @FindBy(xpath = "//*[@id='x-auto-0']/img")
    public static WebElement Password_RequiredField_Validation_Sign;

    @FindBy(xpath = "//*[text()='Login failed. Please try again !']")
	public static WebElement Validation_Message_Text;

    @FindBy(xpath = "//div[@role='menubar'][2]/table/tbody/tr/td[2]")
    public static WebElement Loggedin_Username;

    @FindBy(xpath = "//td[text()='Logout']")
    public static WebElement Logout_Text;



    
	/*****************************
	 * WebElement of default google map screen/dashboard
	 ************************************/

	@FindBy(xpath = "//div[@id='googleMapWidget']")
	public static WebElement Google_Map1;

	@FindBy(xpath = "//table[@class='headerBackground' and @cellspacing='2']")
	WebElement Functional_Toolbar;

	@FindBy(xpath = "//td[@align='left']/following::img[@title='Default Site View']")
	public static WebElement Default_Siteview_Toolbaricon;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Event_History_Close_X;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Alarm_History_Close_X;


	@FindBy(xpath = "html/body/div[8]")
	static WebElement Progress_Popup;

	@FindBy(xpath = "//input[@class='gwt-TextBox']")
	public WebElement Location_SearachBox;
	
/*****************************************Manual Metdata Input WebElement ***********************************************************************/
	

	@FindBy(xpath="html/body/div[2]/div/div[2]/div[3]/div[3]")
	public WebElement Downwind_Places_Window;
	
	@FindBy(xpath="html/body/div[2]/div/div[2]/div[3]/div[3]/div[2]")
	public  WebElement Downwind_Places_Window_Status;
	
	@FindBy(xpath="html/body/div[2]/div/div[2]/div[3]/div[3]/div[1]/div/div[2]/table/tbody/tr/td[1]/div")
	public WebElement ExpandIcon;
	
	
	@FindBy(xpath="html/body/div[2]/div/div[2]/div[3]/div[3]/div[2]/div[1]/div/div/div/div[2]/div/table/tbody[2]/tr")
	public List<WebElement> Downwind_Places_List;

/***********************************************************************************************************************************	
	
	/**************************************
	 * Single Sign on Objects
	 *******************************/
	@FindBy(xpath = ".//*[@type='email']")
	public static WebElement Account_User_Name_Input_Field;
	
	@FindBy(xpath = ".//*[@value='Next']")
	public static WebElement Account_User_Name_Next_Button;
	
	@FindBy(xpath = ".//*[@value='Yes']")
	public static WebElement Account_Acceptance_Yes_Button;
	
	
	@FindBy(xpath = ".//*[@id='userNameInput']")
	public static WebElement Azure_User_Name_Input_Field;
	
	@FindBy(xpath = ".//*[@id='passwordInput']")
	public static WebElement Azure_Password_Input_Field;
	
	@FindBy(xpath = ".//*[@id='submitButton']")
	public static WebElement Azure_Signin_button;
	
	
	/**************************************
	 * Screening Log General Objects 
	 *******************************/
	@FindBy(xpath = ".//*[@routerlink='/welcome']")
	public static WebElement User_Profile_Link;
	
	@FindBy(xpath = ".//*[text()='User Admin']")
	public static WebElement Top_Nav_Admin_Link;
	
	@FindBy(xpath = ".//*[text()='Audit']")
	public static WebElement Top_Nav_Audit_Link;
	
	@FindBy(xpath = ".//*[text()='Logs']")
	public static WebElement Top_Nav_Logs_Link;
	
	@FindBy(xpath = ".//*[text()='Title Management']")
	public static WebElement Top_Nav_Title_Management_Link;
	
	@FindBy(xpath = ".//*[text()='Dropdown Values']")
	public static WebElement Top_Nav_Dropdown_Values_Link;
	
	@FindBy(xpath = ".//*[text()='Logout']")
	public static WebElement Top_Nav_Logout_Link;
	
	@FindBy(xpath = ".//*[@placeholder='Title Filter']//li")
	public static WebElement Title_Filter_List;
	
	@FindBy(xpath = ".//*[@placeholder='Screener Filter']//li")
	public static WebElement Screener_Filter_List;
	
	@FindBy(xpath = ".//*[@placeholder='Status Filter']//li")
	public static WebElement Status_Filter_List;
	
	/**************************************
	 * Screening Log Page Objects 
	 *******************************/
	@FindBy(xpath = ".//*[text()='Trailer']/input")
	public static WebElement Tralier_Radio_Button;
	
	@FindBy(xpath = ".//*[text()='Trailer']/input")
	public static WebElement Feature_Radio_Button;
	
	@FindBy(xpath = ".//*[text()='New Log']")
	public static WebElement New_Log_Button;
	
	@FindBy(xpath = ".//*[text()='Email Selected']")
	public static WebElement Email_Selected_Button;
	
	/**************************************
	 * Screening Log Trailerview Page Objects 
	 *******************************/
	
	@FindBy(xpath = ".//*[@placeholder='Title Filter']//input")
	public static WebElement Title_Filter_Input_Field;
	
	@FindBy(xpath = ".//*[@placeholder='UUID Filter']")
	public static WebElement UUID_Filter_Input_Field;
	
	@FindBy(xpath = ".//*[@placeholder='Screener Filter']//input")
	public static WebElement Screener_Filter_Input_Field;
	
	@FindBy(xpath = ".//*[@placeholder='Status Filter']//input")
	public static WebElement Status_Filter_Input_Field;
	
	@FindBy(xpath = ".//*[@fill='currentColor']")
	public static WebElement Date_Picker_Filter_Input_Field;
	
	@FindBy(xpath = ".//*[@class='mat-checkbox-inner-container']")
	public static WebElement Select_All_Checkbox;
	
	@FindBy(xpath = ".//*[@placeholder='Title Filter']//span")
	public static WebElement Title_Filter_Clear_Link;
	
	@FindBy(xpath = ".//*[@class='fa fa-times content-clear']")
	public static WebElement UUID_Filter_Clear_Link;
	
	@FindBy(xpath = ".//*[@placeholder='Screener Filter']//span")
	public static WebElement Screener_Filter_Clear_Link;
	
	@FindBy(xpath = ".//*[@placeholder='Status Filter']//span")
	public static WebElement Status_Filter_Clear_Link;
	
	@FindBy(xpath = "//span[@class = 'fa fa-times dateclear clear-set']")
	public static WebElement Date_Filter_Clear_Link;
	
	@FindBy(xpath = ".//*[@class='center t-sm']/mat-checkbox/label/div")
	public static WebElement Single_Email_Checkbox;
	
	@FindBy(xpath = ".//*[text()='Title']")
	public static WebElement Title_Sort_Link;
	
	@FindBy(xpath = ".//*[text()='Type']")
	public static WebElement Type_Sort_Link;
	
	@FindBy(xpath = ".//*[text()='Ver']")
	public static WebElement Ver_Sort_Link;
	
	@FindBy(xpath = ".//*[text()='UUID']")
	public static WebElement UUID_Sort_Link;
	
	@FindBy(xpath = ".//*[text()='Start Date']")
	public static WebElement Start_Date_Sort_Link;
	
	@FindBy(xpath = ".//*[text()='Status']")
	public static WebElement Status_Sort_Link;
	
	@FindBy(xpath = ".//*[text()='Screener']")
	public static WebElement Screener_Sort_Link;
	
	@FindBy(xpath = ".//*[text()='Dim']")
	public static WebElement Dim_Sort_Link;
	
	@FindBy(xpath = ".//*[text()='Proj Type']")
	public static WebElement Proj_Type_Sort_Link;
	
	@FindBy(xpath = ".//*[text()='Language']")
	public static WebElement Language_Sort_Link;
	
	@FindBy(xpath = ".//*[text()='Lang Type']")
	public static WebElement Lang_Type_Sort_Link;
	
	@FindBy(xpath = ".//*[text()='Pic Type']")
	public static WebElement Pic_Type_Sort_Link;
	
	@FindBy(xpath = ".//*[text()='Emailed']")
	public static WebElement Emailed_Sort_Link;
	
	@FindBy(xpath = ".//*[@class='dropup']/button")
	public static WebElement Log_View_Display_Setting;
	
	@FindBy(xpath = "//button[contains(@class,'dropdown-item result-btn') and text() = '10']")
	public static WebElement Log_View_Display_Setting_Value_10;
	
	@FindBy(xpath = "//button[contains(@class,'dropdown-item result-btn') and text() = '25']")
	public static WebElement Log_View_Display_Setting_Value_25;
	
	@FindBy(xpath = "//button[contains(@class,'dropdown-item result-btn') and text() = '100']")
	public static WebElement Log_View_Display_Setting_Value_100;
	
	@FindBy(xpath = "//tr[@class = 'ng-star-inserted']")
	public static WebElement Log_View_All_Items_List;
	
	@FindBy(xpath = ".//*[@class='fa fa-angle-right']")
	public static WebElement Log_View_Pagination_Next_Page;
	
	@FindBy(xpath = ".//*[@class='fa fa-angle-double-right]")
	public static WebElement Log_View_Pagination_Last_Page;
	
	@FindBy(xpath = ".//*[@class='fa fa-angle-left']")
	public static WebElement Log_View_Pagination_Previous_Page;
	
	@FindBy(xpath = ".//*[@class='fa fa-angle-double-left']")
	public static WebElement Log_View_Pagination_First_Page;
	
	
	/**************************************
	 * Screening Log Featureview Page Objects 
	 *******************************/
	@FindBy(xpath = ".//*[@placeholder='CTT Filter']")
	public static WebElement CTT_Filter_Input_Field;
	
	@FindBy(xpath = ".//*[@class='fa fa-times content-clear']")
	public static WebElement CTT_Filter_Input_Clear_Link;
	
	@FindBy(xpath = ".//*[text()='CTT']")
	public static WebElement CTT_Sort_link;
	
	@FindBy(xpath = ".//*[text()='Fmt']")
	public static WebElement Fmt_Sort_link;
	
	@FindBy(xpath = ".//*[text()='Type']")
	public static WebElement Type_Sort_link;
	
	@FindBy(xpath = ".//*[text()='Cen Terr']")
	public static WebElement Cen_Terr_Sort_link;
	
	
	/**************************************
	 * Screening Log Trailer Page Objects 
	 *******************************/
	
	@FindBy(xpath = ".//*[@vsendpoint='account/users']//input")
	public static WebElement Screener_Input_Field;
	
	@FindBy(xpath = ".//*[text()='Screener:']")
	public static WebElement Screener_Input_Field_Label;
	
	@FindBy(xpath = ".//*[@vsendpoint='account/users']//span")
	public static WebElement Screener_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/shared-theater']//input")
	public static WebElement Theater_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/shared-theater']//span")
	public static WebElement Theater_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@class='col-lg-7']//textarea")
	public static WebElement Attendees_Input_Field;
	
	@FindBy(xpath = "")
	public static WebElement Start_Date_Select_Field;
	
	@FindBy(xpath = "")
	public static WebElement End_Date_Select_Field;
	
	@FindBy(xpath = ".//*[@vsnendpoint='title/list']//input")
	public static WebElement Title_Input_Field;
	
	@FindBy(xpath = ".//*[@vsnendpoint='title/list']//span")
	public static WebElement Title_Input_Field_Clear_Link;
		
	@FindBy(xpath = ".//*[@vsendpoint='field/active/trailer-type']//input")
	public static WebElement Type_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/trailer-type']//span")
	public static WebElement Type_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/trailer-version']//input")
	public static WebElement Version_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/trailer-version']//span")
	public static WebElement Version_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@class='row']/div[1]/input")
	public static WebElement CTT_Input_Field;
	
	@FindBy(xpath = ".//*[@class='ng-star-inserted']/div/div/div[1]/div[2]/div/div[2]/div[2]/input")
	public static WebElement CPL_UUID_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/shared-dimension']//input")
	public static WebElement Dimension_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/shared-dimension']//span")
	public static WebElement Dimension_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/shared-projector_type']//input")
	public static WebElement Projector_Type_Input_Field;
		
	@FindBy(xpath = ".//*[@vsendpoint='field/active/shared-projector_type']//span")
	public static WebElement Projector_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/trailer-language_type']//input")
	public static WebElement Language_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/trailer-language_type']//span")
	public static WebElement Language_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/trailer-picture_type']//input")
	public static WebElement Picture_Type_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/trailer-picture_type']//span")
	public static WebElement Picture_Type_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/shared-audio_format']//input")
	public static WebElement Audio_Format_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/shared-audio_format']//span")
	public static WebElement Audio_Format_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/trailer-special_track']//input")
	public static WebElement Special_Track_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/trailer-special_track']//span")
	public static WebElement Special_Track_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@class='col-lg-12']/textarea")
	public static WebElement Misc_Comments_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/shared-status']//input")
	public static WebElement Status_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/shared-status']//span")
	public static WebElement Status_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/shared-category']//input")
	public static WebElement Category_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/shared-catrgory']//span")
	public static WebElement Category_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[text()='Save']")
	public static WebElement Log_Save_Button;
	
	@FindBy(xpath = ".//*[text()='Cancel']")
	public static WebElement Log_Cancel_Button;
	
	@FindBy(xpath = ".//*[@class='col ng-star-inserted']//button")
	public static WebElement Log_Delete_Button;
	
	@FindBy(xpath = ".//*[@_ngcontent-c10='']/div[2]/button")
	public static WebElement Log_Delete_Cancel_Button;
		
	@FindBy(xpath = ".//*[text()='Save & Copy']")
	public static WebElement Log_Save_Copy_Button;
	
	@FindBy(xpath = ".//*[text()='Ok']")
	public static WebElement Save_Copy_Duplicate_OK_Button;
	
	/**************************************
	 * Screening Log Feature Page Objects 
	 *******************************/
	@FindBy(xpath = ".//*[@class='screen']/div/div[1]/div[3]/div/div[2]/div[1]/app-dropdown/div/input")
	public static WebElement Language_Region_Input_Field;
	
	@FindBy(xpath = ".//*[@class='container']/div[1]/div[3]/div/div[2]/div[1]/app-dropdown/div/span")
	public static WebElement Language_Region_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@class='container']/div[1]/div[3]/div/div[2]/div[2]/input")
	public static WebElement Language_Other_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/feature-version_type']//input")
	public static WebElement Version_Type_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/feature-version_type']//span")
	public static WebElement Version_Type_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@id='mat-checkbox-1']/label/div")
	public static WebElement Censored_Checkbox;
	
	@FindBy(xpath = ".//*[@class='row align-items-end']/div[5]/app-dropdown/div/input")
	public static WebElement Censor_Territory_Input_Field;
	
	@FindBy(xpath = ".//*[@class='row align-items-end']/div[5]/app-dropdown/div/span")
	public static WebElement Censor_Territory_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/feature-base_picture']//input")
	public static WebElement Base_Picture_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/feature-base_picture']//span")
	public static WebElement Base_Picture_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@class='ng-star-inserted']/div/div/div[1]/div[4]/div/div[1]/div[2]/input")
	public static WebElement Territory_Name_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/feature-countdown_type']/div/input")
	public static WebElement Branding_Type_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/feature-countdown_type']/div/span")
	public static WebElement Branding_Type_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@class='row']/div[4]/app-dropdown/div/input")
	public static WebElement Branding_Language_Input_Field;
	
	@FindBy(xpath = ".//*[@class='row']/div[4]/app-dropdown/div/span")
	public static WebElement Branding_Language_Input_Field_Clear_Field;
	
	@FindBy(xpath = ".//*[@class='container']/div[1]/div[4]/div/div[2]/div[1]/app-dropdown/div/input")
	public static WebElement Audio_Language_Input_Field;
	
	@FindBy(xpath = ".//*[@class='container']/div[1]/div[4]/div/div[2]/div[1]/app-dropdown/div/span")
	public static WebElement Audio_Language_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@class='container']/div[1]/div[4]/div/div[2]/div[2]/input")
	public static WebElement Audio_Language_Details_Input_Field;
	
	@FindBy(xpath = ".//*[@id='mat-checkbox-2']/label/div")
	public static WebElement HI_Checkbox;
	
	@FindBy(xpath = ".//*[@id='mat-checkbox-3']/label/div")
	public static WebElement VI_Checkbox;
	
	@FindBy(xpath = ".//*[@id='mat-checkbox-4']/label/div")
	public static WebElement SL_Checkbox;
	
	@FindBy(xpath = ".//*[@id='mat-checkbox-5']/label/div")
	public static WebElement CC_Checkbox;
	
	@FindBy(xpath = ".//*[@id='mat-checkbox-6']/label/div")
	public static WebElement Inserts_Checkbox;
	
	@FindBy(xpath = ".//*[@id='mat-checkbox-7']/label/div")
	public static WebElement Proofs_Checkbox;
	
	@FindBy(xpath = ".//*[@id='mat-checkbox-8']/label/div")
	public static WebElement Dub_Card_Checkbox;
	
	@FindBy(xpath = ".//*[@id='mat-checkbox-9']/label/div")
	public static WebElement Spot_Check_Checkbox;
	
	@FindBy(xpath = ".//*[@class='col-lg-7']//input")
	public static WebElement Insert_Censor_Notes_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/feature-cqo_card']//input")
	public static WebElement CQO_Card_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/feature-cqo_card']//span")
	public static WebElement CQO_Card_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/feature-optimized_card']//input")
	public static WebElement Optimized_Card_Input_Field;
	
	@FindBy(xpath = ".//*[@vsendpoint='field/active/feature-optimized_card']//span")
	public static WebElement Optimized_Card_Input_Field_Clear_Link;
	
	@FindBy(xpath = ".//*[@class='col-lg-4']//input")
	public static WebElement Other_Cards_Input_Field;
	
	@FindBy(xpath = ".//*[@class='container']/div[1]/div[5]/div/textarea")
	public static WebElement Feature_Misc_Comments_Input_Field;
	
	@FindBy(xpath = ".//*[@class='container']/div[1]/div[6]/div/textarea")
	public static WebElement Feature_Special_Instructions_Input_Field;
	
	
	/*****************************************
	 * Screening Log Title Management Objects
	 ******************************************/
	@FindBy(xpath = ".//*[@class='nav-link text-primary']/i")
	public static WebElement Add_Title_Plus_Link;
	
	@FindBy(xpath = ".//*[@placeholder='[title name]']")
	public static WebElement Title_Name_Input_Field;
	
	@FindBy(xpath = ".//*[@placeholder='SAP F#']")
	public static WebElement Sap_Number_Input_Field;
	
	@FindBy(xpath = ".//*[@id='dropdownMenuButton']")
	public static WebElement Activate_Title_Dropdown_Menu;
	
	@FindBy(xpath = ".//*[@class='ng-star-inserted']/div/div/div[2]/div/div[2]/div/div[1]/button")
	public static WebElement Title_Save_Button;
	
	@FindBy(xpath = ".//*[@class='ng-star-inserted']/div/div/div[2]/div/div[2]/div/div[2]/button")
	public static WebElement Title_Cancel_Button;
	
	@FindBy(xpath = ".//*[@class='ng-star-inserted']/div/div/div[2]/div/div[2]/div/div[3]/button")
	public static WebElement Title_Delete_Button;
	
	
	/**************************************
	 * Go to Menu webelements
	 *******************************/

	@FindBy(xpath = "//td[text()='Go To']")
	public static WebElement GoTomenu_Text;

	@FindBy(xpath = "id('gwt-uid-6')")
	public static WebElement CreateReportmenu_Text;

	@FindBy(xpath = "//td[text()='Events History']")
	public static WebElement EventHistorymenu_Text;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement EventHistory_Window;
	
		
	@FindBy(xpath = "//div[text()='Quick Report']/preceding::div[contains(@class,'Css3ToolStyle-close')][1]")
	public static WebElement Terminate_Window_X;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement Quick_Report_Window;
			
	@FindBy(xpath = "//td[text()='Alarm History']")
	public static WebElement AlarmHistorymenu_Text;

	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement AlarmHistorymenu_Window;
	
	@FindBy(xpath = "//td[text()='Sensor History']")
	public static WebElement SensorHistorymenu_Text;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement SensorHistorymenu_Window;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Sensor_History_Close_X;

	@FindBy(xpath = "//td[text()='Wind Rose']")
	public static WebElement WindRosemenu_Text;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement WindRose_Window;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement WindRose_Close_X;

	@FindBy(xpath = "//td[text()='RAE Chemical']")
	WebElement RAEChemicalmenu_Text;

	
	@FindBy(xpath = "//td[text()='ERG Book']")
	public static WebElement ERGBookmenu_Text;
	
	@FindBy(xpath = "//td[text()='Administration']")
	public static WebElement Administration_Menutext;
	
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement Organizations_List_Window;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Terminate_Org_List_Window_X;
	
	@FindBy(xpath="//td[text()='Users']")
	public static WebElement Users_Menuitem;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement Users_List_Window;
	
	/**************************************
	 * Add Met Station webelements
	 *******************************/
	@FindBy(xpath = "//td[text()='Add Met Station']")
	public static WebElement Add_MetStation_Menutext;
	
	@FindBy(xpath = "//div[text()='Save']")
	public static WebElement Add_MetStation_Save_Button;
	
	@FindBy(xpath = "//label[text()='Type']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Add_MetStation_Type_Error;
	
	@FindBy(xpath = "//label[text()='Encoding']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Add_MetStation_Encoding_Error;
	
	@FindBy(xpath = "//label[text()='Address Type']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Add_MetStation_Adress_Type_Error;
	
	@FindBy(xpath = "//label[text()='Numeric Format']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Add_MetStation_Numeric_Format_Error;
	
	@FindBy(xpath = "//label[text()='Name']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Add_MetStation_Name_Error;
	
	@FindBy(xpath = "//label[text()='Height (ft)']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Add_MetStation_Height_Error;
	
	@FindBy(xpath = "//div[text()='Add New Met Station']/preceding::div[contains(@class,'Css3ToolStyle-close')][1]")
	public static WebElement Add_MetStation_Window_X;
	
	@FindBy(xpath = "//label[text()='Type']/following-sibling::div//input")
	public static WebElement Add_MetStation_Type_Input_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='Modbus']")
	public static WebElement Add_MetStation_Type_Value;
	
	@FindBy(xpath="//label[text()='Name']/following-sibling::div/div/div/input")
	public static WebElement Add_MetStation_Name_Input_Field;
	
	@FindBy(xpath="html/body/div[8]/div[2]/div[1]/div/div/div[1]/div/div/div[3]/div/div[1]/div/div/div/div[text()='Add']")
	public static WebElement Add_MetStation_Add_DAQ_Button;
	
	@FindBy(xpath="html/body/div[9]/div[2]/div[1]/div/div/div[1]/div/div/div[1]/div/div[1]/div/div/input")
	public static WebElement Add_MetStation_DAQ_Name_Field;
	
	@FindBy(xpath=".//*[@id='btnSaveDAQ']/div/div")
	public static WebElement Add_MetStation_Add_DAQ_Save_Button;
	
	@FindBy(xpath="html/body/div[8]/div[2]/div[1]/div/div/div[1]/div/div/div[4]/div/div[1]/div/div/div/div[text()='Add']")
	public static WebElement Add_MetStation_Add_Port_Button;
	

	@FindBy(xpath="html/body/div[9]/div[2]/div[1]/div/div/div[1]/div/div/div[1]/div/div[1]/div/div/div/input")
	public static WebElement Met_Station_Port_Type_Inputbox;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='TCP/IP']")
	public static WebElement Met_Station_Port_Type_Value;
	
	@FindBy(xpath = "//label[text()='Host Name or IP Address']/following-sibling::div/div/div/input")
	public static WebElement Met_Station_Port_IP_Input;
	
	@FindBy(xpath = "html/body/div[9]/div[2]/div[1]/div/div/div[1]/div/div/div[4]/div/div[1]/div/div/div/input")
	public static WebElement Met_Station_Port_Number_Value;
		
	@FindBy(xpath = ".//*[@style='margin: 0px; left: 354px; top: 0px;']/div/div")
	public static WebElement Met_Station_Port_Save_Button;
	
	
	@FindBy(xpath = "html/body/div[8]/div[2]/div[1]/div/div/div[1]/div/div/div[8]/div/div[1]/div/div/input")
	public static WebElement Met_Station_DeviceID_Inputbox;
	
	@FindBy(xpath = "//label[text()='Height (ft)']/following-sibling::div/div/div/div/input")
	public static WebElement Met_Station_HeightFT_Inputbox;
	
	@FindBy(xpath = "//label[text()='Encoding']/following-sibling::div//input")
	public static WebElement Add_MetStation_Encoding_Input_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='ASCII']")
	public static WebElement Add_MetStation_Encoding_Value;
	
	@FindBy(xpath = "//label[text()='Address Type']/following-sibling::div//input")
	public static WebElement Add_MetStation_AddressType_Input_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='Holding Registers']")
	public static WebElement Add_MetStation_AdressType_Value;
	
	@FindBy(xpath = "//label[text()='Numeric Format']/following-sibling::div//input")
	public static WebElement Add_MetStation_NumericFormat_Input_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='Integer Unsigned']")
	public static WebElement Add_MetStation_NumericFormat_Value;
	
	/**************************************
	 * Add Sensor Light webelements
	 *******************************/
	@FindBy(xpath = "//td[text()='Add Place']")
	public static WebElement Add_Place_Menutext;
	
	@FindBy(xpath = "//label[text()='Name']/following-sibling::div/div/div/input")
	public static WebElement Place_Name_Input;
	
	@FindBy(xpath = "//label[text()='Type']/following-sibling::div//input")
	public static WebElement Place_Type_Input_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='Healthcare']")
	public static WebElement Place_Type_Input_Value;
	
	@FindBy(xpath = "//div[contains(@title,'DELETETHISPLACE21')]/following-sibling::div/img")
	public static WebElement Place_On_Map;
	
	
	/**************************************
	 * Add Emission webelements
	 *******************************/
	@FindBy(xpath = "//td[text()='Add Emission Source']")
	public static WebElement Emisison_Source_Menutext;
	
	@FindBy(xpath = "//td[text()='Gas']")
	public static WebElement Emisison_Gas_Source_Menutext;
	
	@FindBy(xpath = "//label[text()='Unit/Area Name']/following-sibling::div//input")
	public static WebElement Gas_Source_Area_Name_Input;
	
	@FindBy(xpath = ".//*[@style='width: 207px;']/div/div")
	public static WebElement Add_Emisison_Source_Chemical_Input_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='ABIETIC ACID - 514103']")
	public static WebElement Add_Emission_Source_Chemical_Input_Field_Value;
	
	@FindBy(xpath = ".//*[text()='Save']")
	public static WebElement Emission_Save_Button;
		
	@FindBy(xpath = "//div[contains(@title,'ABIETIC ACID - Gas')]/following-sibling::div/img")
	public static WebElement Gas_Emisison_On_Map;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ButtonStyle-buttonInner') and text()='Delete']")
	public static WebElement Emission_Delete_Button;
	
	@FindBy(xpath = ".//*[text()='Yes']")
	public static WebElement Emission_Source_Delete_Yes_Button;
	
	@FindBy(xpath = ".//*[text()='Edit']")
	public static WebElement Edit_Place_Menu;
	
	@FindBy(xpath = ".//*[@id='btnDelete']/div/div")
	public static WebElement Place_Delete_Button;
	
	@FindBy(xpath = ".//*[text()='Save']")
	public static WebElement Place_Save_Button;
	
	
	
	/**************************************
	 * Add Sensor Light webelements
	 *******************************/
	@FindBy(xpath = "//td[text()='Add Sensor']")
	public static WebElement Add_Sensor_Menutext;
	
	@FindBy(xpath = "//td[text()='Light - Single Path']")
	public static WebElement Sensor_Type_Menutext;
	
	@FindBy(xpath = "//div[text()='Save']")
	public static WebElement Sensor_Save_Button;
	
	@FindBy(xpath = "/html/body/div[8]/div[2]/div[1]/div/div/div[1]/div/div/div[2]/fieldset/div/div/div[2]/div/div/div")
	public static WebElement Sensor_Chemicals_Add_Button;
	
	@FindBy(xpath = "//label[text()='Name']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Add_Light_Sensor_Name_Error;
	
	@FindBy(xpath = "//label[text()='Type']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Add_Light_Sensor_Type_Error;
	
	@FindBy(xpath = "/html/body/div[8]/div[2]/div[1]/div/div/div[2]/div")
	public static WebElement Add_Light_Sensor_Required_Error;
	
	@FindBy(xpath = "html/body/div[9]/div[1]/div/div[2]")
	public static WebElement Add_Light_Sensor_Window_X;
	
	@FindBy(xpath = "//label[text()='Name']/following-sibling::div/div/div/input")
	public static WebElement Add_Light_Sensor_Name_Input;
	
	@FindBy(xpath = "//label[text()='Type']/following-sibling::div//input")
	public static WebElement Add_Light_Sensor_Type_Input_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='Cerex']")
	public static WebElement Add_Light_Sensor_Type_Input_Value;
	
	@FindBy(xpath = "//label[text()='Sensor Interface']/following-sibling::div//input")
	public static WebElement Add_Light_Sensor_Interface_Input_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='Modbus']")
	public static WebElement Add_Light_Sensor_Interface_Input_Value;
	
	@FindBy(xpath = ".//*[@style='position: relative;']/div[2]/div/div/div")
	public static WebElement Add_Light_Sensor_Add_Sensor_Button;
	
	@FindBy(xpath = "html/body/div[7]/div[1]/div/div[3]")
	public static WebElement Add_Light_Sensor_Edit_Sensor_Window;
	
	@FindBy(xpath = "//label[text()='Chemical']/following-sibling::div/div/div/div/input[@tabindex='5']")
	public static WebElement Add_Light_Sensor_Edit_Sensor_Chemical_Input_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='ABIETIC ACID - 514103']")
	public static WebElement Add_Light_Sensor_Edit_Sensor_Chemical_Input_Field_Value;
	
	@FindBy(xpath = "//label[text()='Channel Tag']/following-sibling::div/div/div/div/input[@tabindex='12']")
	public static WebElement Add_Light_Sensor_Edit_Sensor_Channel_Tag_Input;
	
	@FindBy(xpath = ".//*[@style='position: relative; margin: 0px; width: 330px; left: 335px; top: 0px;']/div[1]/div/div[5]/div/div[1]/div/div/div/input")
	public static WebElement Add_Light_Sensor_Edit_Sensor_Over_Range;
		
	@FindBy(xpath = ".//*[@style='overflow: hidden;']/div/div[3]/div/div/div[2]/div/div")
	public static WebElement Add_Light_Sensor_Edit_SEnsor_Save_Button;
	
	
	/**************************************
	 * Site Settings webelements
	 *******************************/
	
	@FindBy(xpath = "//td[text()='Settings']")
	public static WebElement Settings_Menutext;
				
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement Settings_Window;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Settings_Window_X;
	
	@FindBy(xpath = ".//*[text()='General']")
	public static WebElement Settings_Window_General_Tab;
	
	@FindBy(xpath = ".//*[text()='Limits']")
	public static WebElement Settings_Window_Limits_Tab;
	
	@FindBy(xpath = ".//*[text()='License']")
	public static WebElement Settings_Window_License_Tab;
	
	@FindBy(xpath = "//label[text()='High Wind Speed (m/s)']/following-sibling::div/div/div/div/input")
	public static WebElement Settings_Window_Wind_Speed_Input_Field;
	
	@FindBy(xpath = "//label[text()='High Temperature (deg C)']/following-sibling::div/div/div/div/input")
	public static WebElement Settings_Window_High_Temp_Input_Field;
	
	@FindBy(xpath = "//label[text()='Low Temperature (deg C)']/following-sibling::div/div/div/div/input")
	public static WebElement Settings_Window_Low_Temp_Input_Field;
	
	@FindBy(xpath = "//label[text()='Map Width (px)']/following-sibling::div/div/div/div/input")
	public static WebElement Settings_Window_Map_Width_Input_Field;
	
	@FindBy(xpath = "//label[text()='ERG Data Avg. Interval (min)']/following-sibling::div/div/div/div/input")
	public static WebElement Settings_Window_ERG_Data_Input_Field;
	
	@FindBy(xpath = "//label[text()='Interval for Corridor Length (min)']/following-sibling::div/div/div/div/input")
	public static WebElement Settings_Window_Interval_Corridor_Input_Field;
	
	@FindBy(xpath = "//label[text()='Earth Network Subscription Key']/following-sibling::div/div/div/input")
	public static WebElement Settings_Window_Earth_Netwrork_Key_Input_Field;
	
	@FindBy(xpath = "//label[text()='Max Zoom Level']/following-sibling::div/div/div/div/input")
	public static WebElement Settings_Window_Max_Zoom_Level_Input_Field;
	
	@FindBy(xpath = "//label[text()='Zoom Level for Places']/following-sibling::div/div/div/div/input")
	public static WebElement Settings_Window_Zoom_Level_Places_Input_Field;
	
	@FindBy(xpath = "//label[text()='Zoom Level for Sensors']/following-sibling::div/div/div/div/input")
	public static WebElement Settings_Window_Zoom_Level_Sensors_Input_Field;
	
	@FindBy(xpath = "//label[text()='Low (psi)']/following-sibling::div/div/div/div/input")
	public static WebElement Settings_Window_Low_Psi_Input_Field;
	
	@FindBy(xpath = "//label[text()='Medium (psi)']/following-sibling::div/div/div/div/input")
	public static WebElement Settings_Window_Medium_Psi_Input_Field;
	
	@FindBy(xpath = "//label[text()='High (psi)']/following-sibling::div/div/div/div/input")
	public static WebElement Settings_Window_High_Psi_Input_Field;
	
	@FindBy(xpath = "//label[text()='Low (W/m^2)']/following-sibling::div/div/div/div/input")
	public static WebElement Settings_Window_Thermal_Limits_Low_Input_Field;
	
	@FindBy(xpath = "//label[text()='Medium (W/m^2)']/following-sibling::div/div/div/div/input")
	public static WebElement Settings_Window_Thermal_Limits_Medium_Input_Field;
	
	@FindBy(xpath = "//label[text()='High (W/m^2)']/following-sibling::div/div/div/div/input")
	public static WebElement Settings_Window_Thermal_Limits_High_Input_Field;
	
	@FindBy(xpath = "//label[text()='Lightning']/following-sibling::div/div/div/input[@type='checkbox']")
	public static WebElement Settings_Window_License_Lightning_Checkbox;
	
	
	@FindBy(xpath = ".//*[@style='position: relative; width: 330px;']/div[2]/div/div[1]/div/div/textarea")
	public static WebElement Settings_Description1_Input_Field;
	
	@FindBy(xpath = ".//*[@style='position: relative; width: 330px;']/div[4]/div/div[1]/div/div/textarea	")
	public static WebElement Settings_Description2_Input_Field;
	
	@FindBy(xpath = ".//*[@style='position: relative; width: 330px;']/div[6]/div/div[1]/div/div/textarea	")
	public static WebElement Settings_Description3_Input_Field;
	
	@FindBy(xpath = ".//*[@style='position: relative;']/div[2]/div/div[1]/div/div/textarea")
	public static WebElement Settings_Description4_Input_Field;
	
	@FindBy(xpath = ".//*[@style='position: relative;']/div[4]/div/div[1]/div/div/textarea	")
	public static WebElement Settings_Description5_Input_Field;
	
	@FindBy(xpath = ".//*[@style='position: relative;']/div[6]/div/div[1]/div/div/textarea	")
	public static WebElement Settings_Description6_Input_Field;
	
	@FindBy(xpath = "//div[text()='Save']")
	public static WebElement Settings_Save_Button;
	
	@FindBy(xpath = "//td[text()='Create Report']")
	public  WebElement QRCreatereport_Button;

	@FindBy(xpath = "//div[text()='Create Report']")
	public  WebElement createreport_Button;
	

	
	/**************************************
	 * Add RAE Sensor webelements
	 *******************************/
	@FindBy(xpath = "//td[text()='Add Sensor']")
	public static WebElement Add_RAE_Sensor_Menutext;
	
	@FindBy(xpath = "//td[text()='RAE']")
	public static WebElement Sensor__RAE_Type_Menutext;
	
	@FindBy(xpath = "//div[text()='Save']")
	public static WebElement Sensor_RAE_Save_Button;
	
	@FindBy(xpath = "//label[text()='DAQ']/following-sibling::div/div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Add_RAE_Sensor_DAQ_Error;
	
	@FindBy(xpath = "//label[text()='Port']/following-sibling::div/div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Add_RAE_Sensor_Port_Error;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Add_RAE_Sensor_Window_X;
	
	@FindBy(xpath="//label[text()='Name']/following-sibling::div/div/div/input")
	public static WebElement Add_RAE_Sensor_Name_Input_Field;
	
	@FindBy(xpath="html/body/div[8]/div[2]/div[1]/div/div/div[1]/div/div/div[3]/div/div[1]/div/div/div/div[text()='Add']")
	public static WebElement Add_RAE_Sensor_Add_DAQ_Button;
	
	@FindBy(xpath="html/body/div[8]/div[2]/div[1]/div/div/div[1]/div/div/div[4]/div/div[1]/div/div/div/div[text()='Add']")
	public static WebElement Add_RAE_Sensor_Add_Port_Button;
	
	
	
	
	/**************************************
	 * Add New Organization webelements
	 *******************************/
	
	@FindBy(xpath = "//td[text()='Organizations']")
	public static WebElement Organization_Menutext;
	
	@FindBy(xpath = "/html/body/div[8]/div[2]/div[1]/div/div/div[2]/div/div/div[20]/div/div/div/img")
	public static WebElement Organization_Screen_DropDown;
	
	@FindBy(xpath="/html/body/div[11]/div/div[1]/div/span") 
	public static WebElement Addbutton_Drop_Org_Screen;
	
	@FindBy(xpath="/html/body/div[11]/div/div[2]/div/span") 
	public static WebElement Deletebutton_Drop_Org_Screen;
	
	@FindBy(xpath="html/body/div[8]/div[2]/div[1]/div/div/div[2]/div/div/div[17]/div/div[text()='Add Organization']") 
	public static WebElement Addbutton_Org_Screen;
	
	@FindBy(xpath="//label[text()='Name']/following-sibling::div/div/div/input") 
	public static WebElement Name_Org_Screen;
	
	@FindBy(xpath="//div[text()='Save']")
	public static WebElement Savebutton_addneworg;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement Org_List_Window;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Org_List_Window_X;
	
	@FindBy(xpath=".//*[@class='LIJLHMB-r-b-com-sencha-gxt-theme-base-client-container-HBoxLayoutDefaultAppearance-HBoxLayoutStyle-inner']/div[4]/div/div")
	public static WebElement Delete_Org_Button;
	
	
	
	/**************************************
	 * Add New User webelements
	 *******************************/
	
	@FindBy(xpath="html/body/div[8]/div[2]/div[1]/div/div/div[2]/div/div/div[14]/div/div[text()='Add']") 
	public static WebElement Addbutton_Userslist_Screen;
	
	@FindBy(xpath="//div[text()='Add New User']")
	public static WebElement Addnewuser_screen_Title;
	
	@FindBy(xpath="//div[text()='Edit User']")
	public static WebElement Edit_User_screen_Title;
	
	@FindBy(xpath="//label[text()='First Name']/following-sibling::div/div/div/input")
	public static WebElement Firstname_Inputbox;
	
	@FindBy(xpath="//label[text()='Last Name']/following-sibling::div/div/div/input")
	public static WebElement Lastname_Inputbox;
	
	@FindBy(xpath="//label[text()='Email']/following-sibling::div/div/div/input")
	public static WebElement Email_Inputbox;
	
	@FindBy(xpath="//label[text()='Phone']/following-sibling::div/div/div/input")
	public static WebElement Phone_Inputbox;
	
	@FindBy(xpath="//label[text()='Password']/following-sibling::div/div/div/input")
	public static WebElement Password_Inputbox;
	
	@FindBy(xpath="//label[text()='Confirm Password']/following-sibling::div/div/div/input")
	public static WebElement ConfirmPassword_Inputbox;
	
	@FindBy(xpath="//div[text()='Westlake']")
	public static WebElement Site_Checkbox_Westlake;
	
	@FindBy(xpath="html/body/div[9]/div[2]/div[1]/div/div/div[1]/div/div/div[8]/div/div[1]/div/div/div[2]/div/table/tbody[2]/tr//td[2]")
	public static List<WebElement> ANU_Sites_List;
	
	@FindBy(xpath="//label[text()='Role']/following-sibling::div/div/div/div/input")
	public static WebElement Role_Inputbox;

	@FindBy(xpath="//div[contains(@class,'Css3ListViewStyle-item') and text()='Site Administrator']")
	public static WebElement Role_Value;
	
	@FindBy(xpath="//label[text()='Enabled']/following-sibling::div/div/div/input[@type='checkbox']")
	public static WebElement Enable_Checkbox;
	
	@FindBy(xpath="//div[text()='Save']")
	public static WebElement Savebutton_addnewuser;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Users_List_Window_X;

	@FindBy(xpath = "//div[text()='Add New User']/preceding::div[contains(@class,'Css3ToolStyle-close')][1]")
	public static WebElement Add_User_Window_X;
	
	@FindBy(xpath="//div[contains(@class,'Css3ButtonStyle-buttonInner') and text()='Delete']")
	public static WebElement Delete_User_Button;

	@FindBy(xpath="//label[text()='First Name']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement First_Name_Error;

	@FindBy(xpath="//label[text()='Last Name']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Last_Name_Error;

	@FindBy(xpath="//label[text()='Email']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Email_Error;

	@FindBy(xpath="//label[text()='Password']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Password_Error;

	@FindBy(xpath="//label[text()='Sites']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Sites_Error;

	@FindBy(xpath="//label[text()='Confirm Password']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Confirm_Password_Error;

	@FindBy(xpath="//label[text()='Role']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Role_Error;

	/**************************************
	 * Sites
	 *******************************/
	
	@FindBy(xpath = "//td[text()='Sites']")
	public static WebElement Sites_Menutext;
	
	@FindBy(xpath="html/body/div[8]/div[2]/div[1]/div/div/div[2]/div/div/div[18]/div/div[text()='Add']") 
	public static WebElement Addbutton_Siteslist_Screen;
	
	@FindBy(xpath="//div[text()='Save']")
	public static WebElement Savebutton_addnewsite;
	
	@FindBy(xpath="//label[text()='Name']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Name_Error;
	
	@FindBy(xpath="//label[text()='Surf. Roughness (ft)']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Surf_Error;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement Sites_List_Window;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Sites_List_Window_X;
	
	@FindBy(xpath="html/body/div[8]/div[2]/div[1]/div/div/div[2]/div/div/div[18]/div/div[text()='Add']") 
	public static WebElement Addbutton_Sitelist_Screen;
	
	@FindBy(xpath="//label[text()='Name']/following-sibling::div/div/div/input")
	public static WebElement Sitename_Inputbox;
	
	
	@FindBy(xpath="//label[text()='Surf. Roughness (ft)']/following-sibling::div//input")
	public static WebElement Surf_Inputbox;
	
	@FindBy(xpath="//div[contains(@class,'Css3ListViewStyle-item') and text()='0.33 - Tall grass']")
	public static WebElement Surf_Value;
		
	@FindBy(xpath="//input[@type='checkbox']")
	public static WebElement Enable_Site_Checkbox;
	
	@FindBy(xpath="//div[contains(@class,'Css3ButtonStyle-buttonInner') and text()='Delete']")
	public static WebElement Delete_Site_Button;
	
	@FindBy(xpath="html/body/div[8]/div[2]/div[1]/div/div/div[2]/div/div/div[14]/div/div[text()='Add Site']")
	public static WebElement Add_Site_Button;
	
	@FindBy(xpath="//div[text()='OK']")
	public static WebElement OK_Site_Created_Button;
	
	@FindBy(xpath="//div[text()='Add New User']/preceding::div[contains(@class,'Css3ToolStyle-close')][1]")
	public static WebElement Add_New_User_X;

	@FindBy(xpath="//div[text()='Users List for Org5']/preceding::div[contains(@class,'Css3ToolStyle-close')][1]")
	public static WebElement User_List_for_Org_X;
	
	/**************************************
	 * Data Source/DAQ
	 *******************************/
		
	@FindBy(xpath = "//td[text()='Data Sources']")
	public static WebElement Data_Source_Menutext;
	
	@FindBy(xpath = "//td[text()='DAQ']")
	public static WebElement DAQ_Menutext;
				
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement DAQ_List_Window;
	
	@FindBy(xpath = "//div[text()='Add New DAQ']/preceding::div[contains(@class,'Css3ToolStyle-close')][1]")
	public static WebElement Add_DAQ_List_Window_X;
	
	@FindBy(xpath = "//div[text()='DAQ List']/preceding::div[contains(@class,'Css3ToolStyle-close')][1]")
	public static WebElement DAQ_List_Window_X;
	
	@FindBy(xpath="html/body/div[8]/div[2]/div[1]/div/div/div[2]/div/div/div[14]/div/div[text()='Add']") 
	public static WebElement Addbutton_DAQlist_Screen;
	
	@FindBy(xpath="//div[text()='Save']")
	public static WebElement Savebutton_addnewDAQ;
	
	@FindBy(xpath="//label[text()='Name']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Name_Error_DAQ;
	
	@FindBy(xpath="//label[text()='Sites']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement DAQ_Sites_Error;
	
	@FindBy(xpath=".//*[@id='btnSaveDAQ']/div/div")
	public static WebElement Save_DAQ_Create_Button;
	
	@FindBy(xpath="html/body/div[10]/div[2]/div[1]/div/div/div[3]/div/div/div[1]/div/div")
	public static WebElement Sensor_Save_DAQ_Create_Button;
	
	@FindBy(xpath="html/body/div[9]/div[2]/div[1]/div/div/div[1]/div/div/div[3]/div/div[1]/div/div/div[2]/div/table/tbody[2]/tr/td[1]/div/div")
	public static WebElement DAQ_Site_Value;
		
	@FindBy(xpath="//label[text()='Name']/following-sibling::div/div/div/input")
	public static WebElement DAQname_Inputbox;
	
	@FindBy(xpath="html/body/div[10]/div[2]/div[1]/div/div/div[1]/div/div/div[1]/div/div[1]/div/div/input")
	public static WebElement Sensor_DAQname_Inputbox;
	
	
	@FindBy(xpath=".//*[@id='btnDeleteDAQ']/div/div")
	public static WebElement DAQ_Delete_Button;
	
	@FindBy(xpath="html/body/div[*]/div[2]/div[2]/div/div/div[1]/div/div")
	public static WebElement DAQ_Yes_Button;
	
	/**************************************
	 * Data Source/Met Station
	 *******************************/
	
	@FindBy(xpath = "//td[text()='Met Stations']")
	public static WebElement Met_Stations_Menutext;
	
	@FindBy(xpath = "/html/body/div[8]/div[2]/div[1]/div/div/div[3]/div/div/div[2]/div/div")
	public static WebElement Met_Stations_Delete_Button;
	
				
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement Met_Stations_List_Window;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Met_Stations_List_Window_X;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ButtonStyle-buttonInner') and text()='Delete']")
	public static WebElement Delete_Met_Station_Button;
	
	@FindBy(xpath="html/body/div[*]/div[2]/div[2]/div/div/div[1]/div/div")
	public static WebElement Met_Station_Yes_Button;
	
	/**************************************
	 * Data Source/Sensor Interfaces
	 *******************************/
	
	@FindBy(xpath = "//td[text()='Sensor Interfaces']")
	public static WebElement Sensor_Interfaces_Menutext;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ButtonStyle-buttonInner') and text()='Delete']")
	public static WebElement Delete_Sensor_Button;
					
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement Sensor_Interfaces_List_Window;
	
	@FindBy(xpath = "/html/body/div[8]/div[2]/div[1]/div/div/div[2]/div/div/div[3]/div/div")
	public static WebElement Sensor_Interfaces_Add_Button;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Sensor_Interfaces_List_Window_X;
	
	@FindBy(xpath = "html/body/div[9]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Add_Sensor_Window_X;
	
	@FindBy(xpath = "html/body/div[8]/div[2]/div[1]/div/div/div[2]/div/div/div[3]/div/div[text()='Add']")
	public static WebElement Sensor_Interface_Add_Button;
	
	@FindBy(xpath = "//div[text()='Save']")
	public static WebElement Sensor_Interface_Save_Button;
	
	@FindBy(xpath = "html/body/div[9]/div[2]/div[1]/div/div/div[1]/div/div/div[3]/div/div[1]/div/div/div/div[text()='Add']")
	public static WebElement Add_DAQ_Button;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='Boreal']")
	public static WebElement Sensor_Interface_Type_Field;
	
	@FindBy(xpath = "//label[text()='Type']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Sensor_Interface_Type_Field_Error;
	
	@FindBy(xpath="//label[text()='Type']/following-sibling::div/div/div/div/input")
	public static WebElement Type_Inputbox;
	
	@FindBy(xpath="//div[contains(@class,'Css3ListViewStyle-item') and text()='Modbus']")
	public static WebElement Sensor_Type_Value;
	
	@FindBy(xpath = "//label[text()='Name']/following-sibling::div/div/div/input")
	public static WebElement Sensor_Interface_Name_Field;
	
	@FindBy(xpath = "//label[text()='Name']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Sensor_Interface_Name_Field_Error;
	
	@FindBy(xpath="/html/body/div[9]/div[2]/div[1]/div/div/div[1]/div/div/div[3]/div/div[1]/div[@id='x-auto-294']/div[@id='x-auto-293']/div/div/div")
	public static WebElement DAQ_Inputbox;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='DAQ3']")
	public static WebElement Sensor_Interface_DAQ_Field;
	
	@FindBy(xpath = "/html/body/div[9]/div[2]/div[1]/div/div/div[1]/div/div/div[3]/div/div[1]/div[@id='x-auto-50']/div[@id='x-auto-49']/img")
	public static WebElement Sensor_Interface_DAQ_Field_Error;
	
	@FindBy(xpath = "html/body/div[9]/div[2]/div[1]/div/div/div[1]/div/div/div[4]/div/div[1]/div/div/div/div[text()='Add']")
	public static WebElement Sensor_Interface_Add_Port_Button;
	
	@FindBy(xpath="html/body/div[10]/div[2]/div[1]/div/div/div[1]/div/div/div[1]/div/div[1]/div/div/div/input")
	public static WebElement Port_Type_Inputbox;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='TCP/IP']")
	public static WebElement Sensor_Interface_Port_Type_Value;
	
	@FindBy(xpath = "//label[text()='Host Name or IP Address']/following-sibling::div/div/div/input")
	public static WebElement Sensor_Interface_Port_IP_Input;
	
	@FindBy(xpath = "/html/body/div[10]/div[2]/div[1]/div/div/div[1]/div/div/div[4]/div/div[1]/div[@id='x-auto-174']/div/div/input[@id='x-auto-174-input']")
	public static WebElement Sensor_Interface_Port_Number_Value;
	
	@FindBy(xpath = "/html/body/div[10]/div[2]/div[1]/div/div/div[1]/div/div/div[9]/div/div[1]/div[@id='x-auto-175']/div/div/input[@id='x-auto-175-input']")
	public static WebElement Sensor_Interface_Port_Read_TimeOut_Field;
	
	@FindBy(xpath = ".//*[@style='margin: 0px; left: 354px; top: 0px;']/div/div")
	public static WebElement Sensor_Interface_Port_Save_Button;
	
	@FindBy(xpath = "//label[text()='Device ID']/following-sibling::div/div/div/input")
	public static WebElement Sensor_Interface_DeviceID_Input_Field;
	
	@FindBy(xpath = "//label[text()='Name']/following-sibling::div/div/div/input")
	public static WebElement Sensor_Interface_Name_Input_Field;
		
	@FindBy(xpath = "//label[text()='Encoding']/following-sibling::div//input	")
	public static WebElement Sensor_Interface_Encoding_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='ASCII']")
	public static WebElement Sensor_Interface_Encoding_Value;
	
	@FindBy(xpath = "//label[text()='Address Type']/following-sibling::div//input")
	public static WebElement Sensor_Interface_AddressType_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='Holding Registers']")
	public static WebElement Sensor_Interface_AddressType_Value;
	
	@FindBy(xpath = "//label[text()='Numeric Format']/following-sibling::div//input")
	public static WebElement Sensor_Interface_NumericFormat_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='Integer Signed']")
	public static WebElement Sensor_Interface_NumericFormat_Value;
	
	@FindBy(xpath = "/html/body/div[9]/div[2]/div[1]/div/div/div[1]/div/div/div[4]/div/div[1]/div[@id='x-auto-113']/div[@id='x-auto-112']/img")
	public static WebElement Sensor_Interface_Port_Field_Error;
	
	@FindBy(xpath = "//label[text()='Encoding']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Sensor_Interface_Encoding_Field_Error;
	
	@FindBy(xpath = "//label[text()='Address Type']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Sensor_Interface_AddressType_Field_Error;
	
	@FindBy(xpath = "//label[text()='Numeric Format']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Sensor_Interface_NumericFormat_Field_Error;
	
	/**************************************
	 * Data Source/RAE Devices
	 *******************************/
	@FindBy(xpath = "//td[text()='RAE Devices']")
	public static WebElement RAE_Devices_Menutext;
				
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement RAE_Devices_List_Window;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement RAE_Devices_List_Window_X;
	
	/**************************************
	 * Data Source/Light Devices
	 *******************************/
	@FindBy(xpath = "//td[text()='Light Devices']")
	public static WebElement Light_Devices_Menutext;
				
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement Light_Devices_List_Window;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Light_Devices_List_Window_X;
	
	@FindBy(xpath = ".//*[@id='btnDelete']/div/div")
	public static WebElement Light_Devices_Delete_Button;
	
	@FindBy(xpath = ".//*[text()='Yes']")
	public static WebElement Light_Devices_Delete_Yes_Button;
	
	
	/**************************************
	 * Data Source/Sensors
	 *******************************/
	@FindBy(xpath = "//td[text()='All Sensors']")
	public static WebElement All_Sensors_Menutext;
				
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement All_Sensors_List_Window;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement All_Sensors_List_Window_X;
	
	@FindBy(xpath = "//td[text()='Current Site']")
	public static WebElement Current_Site_Menutext;
	
		
	@FindBy(xpath = "//td[text()='Units']")
	public static WebElement Units_Menutext;
				
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement Units_Window;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Units_Window_X;
		
	
	/**************************************
	 * Scenarios Objects/Data Elements
	 *******************************/
	
	@FindBy(xpath = "//td[text()='Scenarios']")
	public static WebElement Scenarios_Menutext;
				
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement Scenarios_Window;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Scenarios_Window_X;
	
	@FindBy(xpath = ".//*[@class='margin-10']/div/div[2]/div/div/div[3]/div/div")
	public static WebElement Scenarios_Add_Button;
	
	@FindBy(xpath = "//SPAN[@class='LIJLHMB-C-b-com-sencha-gxt-theme-neptune-client-base-menu-Css3MenuItemAppearance-Css3MenuItemStyle-menuItem'][text()='Liquid Release']")
	public static WebElement Scenario_Add_Value;
	
	@FindBy(xpath = ".//*[text()='Create Liquid Release']")
	public static WebElement Create_Liquid_Release_Window;
	
	@FindBy(xpath = "//label[text()='Unit/Area Name']/following-sibling::div//input")
	public static WebElement Chemimcal_Liquid_Area_Name_Input;
		
	@FindBy(xpath = ".//*[@style='position: relative; margin: 0px; width: 330px; left: 0px; top: 0px;']/div[7]/div/div/div/div[1]/div/div/div/div")
	public static WebElement Chemimcal_Liquid_Release_Chemical_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='ABIETIC ACID - 514103']")
	public static WebElement Chemimcal_Liquid_Release_Chemical_Value;
	
	@FindBy(xpath = "//label[text()='Release Rate (lb/min)']/following-sibling::div//input")
	public static WebElement Chemimcal_Liquid_Release_Rate_Input;
	
	@FindBy(xpath = ".//*[text()='Manage Predefined Scenarios']")
	public static WebElement Predefined_Scenarios_Window;
	
	@FindBy(xpath = "html/body/div[9]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Predefined_Scenarios_Window_X;
		
	@FindBy(xpath = ".//*[@class='margin-10']/div/div[2]/div/div/div[4]/div/div")
	public static WebElement Predefined_Scenarios_Delete_Button;
		
	@FindBy(xpath = ".//*[text()='Save']")
	public static WebElement Chemimcal_Liquid_Release_Save_Button;
		
	
	
	@FindBy(xpath = "//td[text()='Templates']")
	public static WebElement Templates_Menutext;
				
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement Templates_Window;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Templates_Window_X;
	
	@FindBy(xpath = "//td[text()='All Feedback']")
	public static WebElement All_Feedback_Menutext;
				
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement All_Feedback_Window;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement All_Feedback_Window_X;
	
	
	@FindBy(xpath = "//td[text()='Quick Tip']")
	public static WebElement Quick_Tip_Menutext;
				
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement Quick_Tip_Window;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Quick_Tip_Window_X;
	
	
	/**************************************
	 * Chemicals Objects
	 *******************************/
	
	@FindBy(xpath = "//td[text()='Chemicals']")
	public static WebElement Chemicals_Menutext;
				
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement Chemicals_Window;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ButtonStyle-buttonInner') and text()='Delete']")
	public static WebElement Delete_Chemical_Button;
	
	@FindBy(xpath = ".//*[text()='Add Chemical']")
	public static WebElement Add_Chemicals_Window;
		
	@FindBy(xpath = "//div[text()='Chemicals']/preceding::div[contains(@class,'Css3ToolStyle-close')][1]")
	public static WebElement Chemicals_Window_X;
	
	@FindBy(xpath = "//div[text()='Add Chemical']/preceding::div[contains(@class,'Css3ToolStyle-close')][1]")
	public static WebElement Add_Chemicals_Window_X;
	
	@FindBy(xpath = "//div[text()='Edit Chemical']/preceding::div[contains(@class,'Css3ToolStyle-close')][1]")
	public static WebElement Edit_Chemicals_Window_X;
	
	@FindBy(xpath = ".//*[@class='margin-10']/div/div[3]/div/div/div[4]/div/div")
	public static WebElement Chemicals_Add_Button;
	
	@FindBy(xpath = "//label[text()='Name']/following-sibling::div/div/img[contains(@class,'Css3SideErrorStyle-invalid')]")
	public static WebElement Chemical_Name_Field_Error;
	
	@FindBy(xpath="//label[text()='Name']/following-sibling::div/div/div/input") 
	public static WebElement Chemical_Name_Input;
	
	@FindBy(xpath="//label[text()='CAS']/following-sibling::div/div/div/input") 
	public static WebElement Chemical_CAS_Input;
	
	@FindBy(xpath="//label[text()='UN/DOT']/following-sibling::div/div/div/input") 
	public static WebElement Chemical_UNDOT_Input;
	
	@FindBy(xpath="//label[text()='Formula']/following-sibling::div/div/div/input") 
	public static WebElement Chemical_Formula_Input;
	
	@FindBy(xpath="//label[text()='Alternate Name']/following-sibling::div/div/div/input") 
	public static WebElement Chemical_Alternate_Name_Input;
	
	@FindBy(xpath = "//label[text()='Flammable']/following-sibling::div//input")
	public static WebElement Chemimcal_Flammable_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='NO']")
	public static WebElement Chemimcal_Flammable_Field_Value;
	
	@FindBy(xpath = "//label[text()='Category']/following-sibling::div//input")
	public static WebElement Chemimcal_Category_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='Particulate']")
	public static WebElement Chemimcal_Category_Field_Value;
	
	@FindBy(xpath = ".//*[text()='Particulate properties']")
	public static WebElement Chemicals_Window_Particulate_Properties_Tab;
	
	@FindBy(xpath = "//label[text()='Density (lb/ft ^3)']/following-sibling::div//input")
	public static WebElement Chemimcal_Density_Field;
	
	@FindBy(xpath = "//label[text()='Form']/following-sibling::div//input")
	public static WebElement Chemimcal_Form_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='Liquid']")
	public static WebElement Chemimcal_Form_Field_Value;
			
	@FindBy(xpath = "//label[text()='Diameter type']/following-sibling::div//input")
	public static WebElement Chemimcal_Diamter_Field;
	
	@FindBy(xpath = "//div[contains(@class,'Css3ListViewStyle-item') and text()='Uniform (mean diameter)']")
	public static WebElement Chemimcal_Diamter_Field_Value;
	
	@FindBy(xpath = "//label[text()='Mean Diameter (micrometer)']/following-sibling::div//input")
	public static WebElement Chemimcal_Mean_Diamter_Field;
	
	
		
	@FindBy(xpath = ".//*[@style='position: relative;']/div[3]/div/div/div[3]/div/div")
	public static WebElement Save_Chemical_Button;
		
	
	
	/***********************************
	 * Safer Admin Top Nav Objects
	 *********************************************************/
	
	@FindBy(xpath = "//td[text()='Safer Admin']")
	public static WebElement Safer_Admin_Menutext;
	
	@FindBy(xpath = "//td[text()='Reset to Site Units']")
	public static WebElement Reset_Site_Units_Menutext;
	
	@FindBy(xpath = "//td[text()='My Profile']")
	public static WebElement My_Profile_Menutext;
						
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement My_Profile_Window;
	
	@FindBy(xpath = "//label[text()='Phone']/following-sibling::div/div/div/input")
	public static WebElement My_Profile_Phone_Number_Input;
	
	@FindBy(xpath = ".//*[text()='Simi Valley']")
	public static WebElement My_Profile_Sites_SimiValley;
	
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement My_Profile_Window_X;
	
	@FindBy(xpath = "//td[text()='Change Password']")
	public static WebElement Change_Password_Menutext;
						
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[3]")
	public static WebElement Change_Password_Window;
	
	@FindBy(xpath = "//label[text()='Old Password']/following-sibling::div/div/div/input")
	public static WebElement Change_Password_Old_Password_Input;
	
	@FindBy(xpath = "//label[text()='New Password']/following-sibling::div/div/div/input")
	public static WebElement Change_Password_New_Password_Input;
	
	@FindBy(xpath = "//label[text()='Confirm Password']/following-sibling::div/div/div/input")
	public static WebElement Change_Password_Confirm_Password_Input;
	
	@FindBy(xpath = ".//*[text()='Save']")
	public static WebElement Change_Password_Save_Button;
	
	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Change_Password_Window_X;
	
	
	@FindBy(xpath = "//td[text()='Logout']")
	public static WebElement Logout_Menutext;
	
	/***********************************
	 * Safer Admin Map Tools Items
	 *********************************************************/
	
	@FindBy(xpath = "/html/body/div[2]/div/div[2]/div[2]/div/div[2]/div[1]/table/tbody/tr/td[1]/img")
	public static WebElement Default_Site_View;
	
	@FindBy(xpath = "/html/body/div[2]/div/div[2]/div[2]/div/div[2]/div[1]/table/tbody/tr/td[2]/img")
	public static WebElement Map_Items;
	
	@FindBy(xpath = ".//*[text()='Map Items']")
	public static WebElement Map_Items_Window;
	
	@FindBy(xpath = "//*[@id='x-auto-11']/div[2]/table/tbody/tr/td[2]/div")
	public static WebElement Map_Items_Window_X;
	
	
	@FindBy(xpath = ".//*[@title='Met History']")
	public static WebElement Met_History;
	
	@FindBy(xpath = "//*[@id='x-auto-11']/div[2]/table/tbody/tr/td[2]/div")
	public static WebElement Met_History_Window_X;
	
	@FindBy(xpath = ".//*[@title='Quick Report']")
	public static WebElement Quick_Report;
	
	@FindBy(xpath = ".//*[text()='Quick Report']")
	public static WebElement Quick_Report_Popup;
	
	@FindBy(xpath = "//div[text()='Quick Report']/preceding::div[contains(@class,'Css3ToolStyle-close')][1]")
	public static WebElement Quick_Report_Window_X;
	

	/****************************************
	 * Event History screen
	 ******************************************************/

	@FindBy(xpath = "//td[text()='Events History']")
	public  WebElement QREventHistory_Button;

	
	@FindBy(xpath = "//span[text()='Gas Release']")
	public WebElement Gas_Release_Event;

	@FindBy(xpath = "//div[text()='Gas Release Event']")
	public WebElement Gas_Release_Report;
	
	@FindBy(xpath = "//td[text()='Met History']")
	public WebElement MetHistorymenu_Text;

	@FindBy(xpath = "//div[contains(text(), '5-Minute Average Met History - Modbus')]")
	public WebElement MetHistory_Report;


	
	/***************************************
	 * Map location menu WebElement
	 *******************************************/

	@FindBy(xpath = "//div[@id='googleMapWidget']")
	public WebElement Google_Map;


	@FindBy(xpath = "html/body/div[2]/div/div[2]/div[1]/div[4]/div/div/div/div[1]/div[4]/div[4]/div[2]/img")
	public static WebElement MP_Closebutton;



	/**********************************************************
	 * Dispresion Gas Release event webelements
	 **********************************************************/

	@FindBy(xpath = "html/body/div[8]/div[1]/div/div[2]/table/tbody/tr/td/div")
	public static WebElement Screen_Closebutton;


	@FindBy(xpath = "html/body/div[9]")
	public WebElement Errormsg_popup;

	@FindBy(xpath = "//div[text()='OK']")
	public WebElement Errormsg_popup_Okbutton;


	/***********************************************************
	 * ERG Event menu item WebElement
	 ***************************************************************/
	@FindBy(xpath = "//td[text()='ERG']")
	WebElement NE_ERG_MenuText;

	@FindBy(xpath = "html/body/div[8]")
	public WebElement EventClose_Confirmation_Popup;

	@FindBy(xpath = "//div[text()='Yes']")
	public WebElement Yes_Button;

	@FindBy(xpath = "//div[text()='Close']")
	public WebElement Event_Close_Button;



	/*********************************************************
	 * Add New Sensor Interface Screen WebElements
	 *******************************************************************/

	@FindBy(xpath = "html/body/div[9]/div[2]/div[1]/div/div/div[1]/div/div/div[9]/div/div[1]/div/div[1]/div/div/div/input")
	public WebElement Encoding_Dropdown;

	@FindBy(xpath = "html/body/div[9]/div[2]/div[1]/div/div/div[1]/div/div/div[9]/div/div[2]/div/div[1]/div/div/div/input")
	public WebElement Address_Type_Dropdown;

	@FindBy(xpath = "html/body/div[9]/div[2]/div[1]/div/div/div[1]/div/div/div[9]/div/div[3]/div/div[1]/div/div/div/input")
	public WebElement Numeric_Format_Dropdown;



	/*********************************************************
	 * Add New PORT Screen WebElements
	 *******************************************************************/

	@FindBy(xpath = "html/body/div[10]/div[2]/div[1]/div/div/div[1]/div/div/div[1]/div/div[1]/div/div/div/input")
	public static WebElement Port_Type_Dropdown;

	@FindBy(xpath = "html/body/div[10]/div[2]/div[1]/div/div/div[1]/div/div/div[2]/div/div[1]/div/div/input")
	public WebElement Serial_DeviceName_Inputbox;

	@FindBy(xpath = "html/body/div[10]/div[2]/div[1]/div/div/div[1]/div/div/div[5]/div/div[1]/div/div/div/input")
	public static WebElement Baud_Rate_Dropdown;

	@FindBy(xpath = "html/body/div[10]/div[2]/div[1]/div/div/div[1]/div/div/div[6]/div/div[1]/div/div/div/input")
	public static WebElement Data_Bits_Dropdown;

	@FindBy(xpath = "html/body/div[10]/div[2]/div[1]/div/div/div[1]/div/div/div[7]/div/div[1]/div/div/div/input")
	public static WebElement Parity_Dropdown;

	@FindBy(xpath = "html/body/div[10]/div[2]/div[1]/div/div/div[1]/div/div/div[8]/div/div[1]/div/div/div/input")
	public static WebElement Stop_Bits_Dropdown;

	@FindBy(xpath = "html/body/div[10]/div[2]/div[1]/div/div/div[3]/div/div/div[1]/div/div")
	public static WebElement Add_NewPort_Save_Button;




	/************************
	 * Add Met Station WebElements
	 *************************/

	@FindBy(xpath = "html/body/div[8]/div[2]/div[1]/div/div/div[1]/div/div/div[3]/div/div[1]/div/div[3]/div/div")
	public WebElement Metstation_Add_Daq_Button;

	@FindBy(xpath = "html/body/div[9]/div[2]/div[1]/div/div/div[1]/div/div/div[1]/div/div[1]/div/div/input")
	public WebElement Metstation_DaqName_Inputbox;

	@FindBy(xpath = "html/body/div[9]/div[2]/div[1]/div/div/div[3]/div/div/div[1]/div/div")
	public WebElement Metstation_Daq_Save_Button;


	@FindBy(xpath = "//div[text()='Save']")
	public WebElement Save_Button;

	/*********************
	 * Move Corridor Here webelements
	 *************************/

	@FindBy(xpath = "//td[@align='left']/following::img[@title='Default Site View']")
	public WebElement defaultsite_icon;


	@FindBy(xpath = "//div[text()='Acknowledge']")
	public WebElement Alarm_Acknowledge_Btn;
	

		

	
	

	
	

}
