package com.screeninglog.test;

import com.screeninglog.appLib.ScreeningLogApplLib;
import com.screeninglog.objLib.*;
import com.screeninglog.util.PageUtil;

import java.net.MalformedURLException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class BasicUserToolsAvailabilityTest extends BaseTest {

	ScreeningLogApplLib screeninglogAdmin;

    @BeforeClass(alwaysRun = true)
    public void setup() throws MalformedURLException{
        super.testSetup();
        screeninglogAdmin = new ScreeningLogApplLib(webDriver, softAssert);
        getPage(ScreeningLogObjLib.pageUrl);
        screeninglogAdmin.loginToScreeningLogBasicUser(ScreeningLogObjLib.basicusername, ScreeningLogObjLib.basicuserpassword);
       
       
    }


    @Test(groups = { "smoke","full","test","G"})
     public void Login_validation() {
        PageUtil.waitForPageToLoad();
        screeninglogAdmin.User_Tools_Availability();
        softAssert.assertAll();
    }


}

  	