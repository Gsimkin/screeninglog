package com.screeninglog.test;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.asserts.SoftAssert;

import com.screeninglog.objLib.ScreeningLogObjLib;
import sun.security.krb5.internal.crypto.Des;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    protected WebDriver webDriver;
    protected WebDriverWait wait;
    protected SoftAssert softAssert;

    @BeforeClass(alwaysRun = true)
    protected void testSetup() throws MalformedURLException {
        softAssert = new SoftAssert();
        DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
        WebDriverManager.chromedriver().setup();
        String remote = System.getProperty("remote");
        if(webDriver==null) {
            if(remote.equals("true")) {
                webDriver = new RemoteWebDriver(new URL("http://10.2.0.26:4444/wd/hub"), desiredCapabilities);//test
                 } else
                 {
            webDriver = new ChromeDriver();
                 }
            webDriver.manage().window().maximize();
            wait = new WebDriverWait(webDriver, 10);
            webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
    }
    @AfterClass(alwaysRun = true)
    protected void testCleanup() {
        if(webDriver != null) {
            webDriver.quit();
            webDriver = null;
        }
    }

    protected void getPage(String url) {
        if (!StringUtils.isBlank(url)) {
            webDriver.get(ScreeningLogObjLib.URLStartString + ScreeningLogObjLib.URLAddress + url);
            SessionId session = ((ChromeDriver)webDriver).getSessionId();
            System.out.println("Session id: " + session.toString());
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}